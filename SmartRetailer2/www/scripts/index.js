﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.
var logFile;
var gaPlugin;
(function () {
    "use strict";

    document.addEventListener('deviceready', onDeviceReady.bind(this), false);

    function onDeviceReady() {
            gaPlugin = window.plugins.gaPlugin;
            gaPlugin.init(nativePluginResultHandler, nativePluginErrorHandler, "UA-81620516-1");
            PageButtonClicked("Consumer Home");
        // Handle the Cordova pause and resume events
        document.addEventListener('pause', onPause.bind(this), false);
        document.addEventListener('resume', onResume.bind(this), false);
        var Pushbots;
        document.addEventListener('deviceready', function () {
            Pushbots = PushbotsPlugin.initialize("56a64a3317795906438b456a", { "android": { "sender_id": "942601407343" } });
        }, false);

        Pushbots.on("registered", function (token) {
            localStorage['token'] = token;
            console.log(token);

        });

        Pushbots.on("notification:received", function (data) {
            localStorage['notificationLog'] = JSON.stringify(data);
        });

        Pushbots.getRegistrationId(function (token) {
            console.log("Registration Id:" + token);
        });

        //window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function (dir) {
        //    dir.getFile("log.txt", { create: true }, function (file) {
        //        logFile = file;
        //    });
        //});

        var wentOffline = false;
        document.addEventListener("offline", function () {
            $('#conn-err-img').show();
            if (wentOffline) {
                wentOffline = true;
                navigator.notification.alert(
                'You went offline! Please connect to internet to continue.',
                function () {
                    wentOffline = false;
                },
                'Network Status',
                'OK'
                );
            }
        }, false);
        document.addEventListener("online", function () {
            $('#conn-err-img').hide();
        }, false);
        gaPlugin = window.plugins.gaPlugin;

        //Image cache option settings//
        //ImgCache.options.debug = true;
        //ImgCache.options.chromeQuota = 50 * 1024 * 1024;
        //ImgCache.init(function () {
        //    console.log('ImgCache init: success!');
        //}, function () {
        //    console.log('ImgCache init: error! Check the log for errors');
        //});
        /*******************************/
    };

    function onPause() {
    };

    function onResume() {
    };
})();

function log(text) {
    if (!logFile) return;
    var line = new Date() + ' ' + text + "\n";
    logFile.createWriter(function (fileWriter) {
        fileWriter.seek(fileWriter.length);
        var blob = new Blob([line], { type: 'text/plain' });
        fileWriter.write(blob);
    });
}

function permissionCallback(button) {
    //if (button === 1)
        //gaPlugin.init(nativePluginResultHandler, nativePluginErrorHandler, "UA-75882159-1", 10);
}

function nativePluginResultHandler(result) {

}

function nativePluginErrorHandler(error) {

}

function TrackButtonClicked() {
    gaPlugin.trackEvent(nativePluginResultHandler, nativePluginErrorHandler, "Button", "Click", "event only", 1);
}
function PageButtonClicked(pageName) {
    gaPlugin.trackPage( nativePluginResultHandler, nativePluginErrorHandler, pageName);
}

function isDevice() {
    var app = document.URL.indexOf('http://') === -1 && document.URL.indexOf('https://') === -1;
    if (app) {
        return true;
    } else {
        return false;
    }
}
function httpError(err) {
    console.log(err);
}
function parseDate(date) {
    return new Date(
        date.substring(6, 10),
        date.substring(3, 5),
        date.substring(0, 2),
        date.substring(11, 13),
        date.substring(14, 16),
        date.substring(17, 19)
        );
}
function minifyDate(date) {
    var todayBegin = new Date();
    todayBegin.setHours(0);
    todayBegin.setMinutes(0);
    todayBegin.setSeconds(0);
    if (todayBegin < date) {
        return date.toTimeString().substring(0, 5);
    }
    var yesterdayBegin = todayBegin.setHours(-24);
    if (yesterdayBegin < date) {
        return 'YESTERDAY';
    }
    return date.toDateString().substring(4);
}