﻿(function () {
    "use strict";

    angular.module("myapp.services", [])
    .factory("myappService", ["$rootScope", "$http", function ($rootScope, $http) {
        var myappService = {};

        //starts and stops the application waiting indicator
        myappService.wait = function (show) {
            if (show)
                $(".spinner").show();
            else
                $(".spinner").hide();
        };
        return myappService;
    }]);
    angular.module('myapp.utils', [])
    .factory('$customlocalstorage', ['$window', function ($window) {

        var $customlocalstorage = {
            set: function (key, value) {
                $window.localStorage[key] = value;
            },
            get: function (key, defaultValue) {
                return $window.localStorage[key] || defaultValue;
            },
            setObject: function (key, value) {
                $window.localStorage[key] = JSON.stringify(value);
            },
            getObject: function (key) {
                return JSON.parse($window.localStorage[key] || '{}');
            },
            getObjectorDefault: function (key, defaultValue) {
                return JSON.parse($window.localStorage[key] || defaultValue);
            }
        };

        return $customlocalstorage;
    }])
    .factory('$categoryTree', ['$http', function ($http) {
        var category = [];
        $http({
            method: "GET",
            url: "http://192.168.1.40:8080/category",
        }).then(function (res) {
            category = res.data;
            angular.forEach(category, function (value, index) {
                $http({
                    method: "GET",
                    url: "http://192.168.1.40:8080/category/segment/" + value.id
                }).then(function (segmentsRes) {
                    category[index].segments = segmentsRes.data;
                    angular.forEach(category[index].segments, function (value2, index2) {
                        $http({
                            method: "GET",
                            url: "http://192.168.1.40:8080/category/segment/subsegment/" + value2.id
                        }).then(function (subsegmentsRes) {
                            category[index].segments[index2].subsegment = subsegmentsRes.data;
                        });
                    });
                });
            });
        }, function () {
        });
        return {
            getTree: function () {
                return category;
            },
        };
    }])
    .factory('$popupService', ['$ionicPopup', '$timeout', '$http', function ($ionicPopup, $timeout, $http) {
        return {
            showConfirm: function (title, template, data) {

                var confirm = $ionicPopup.show({
                    template: template,
                    title: title,
                    buttons: [{
                        text: 'No',
                        type: 'button-assertive',
                        onTap: function (e) {
                            data.confirm = false;
                            console.log(false);
                        }
                    }, {
                        text: '<b>Yes</b>',
                        type: 'button-positive',
                        onTap: function (e) {
                            data.confirm = true;
                            console.log(true);
                        }
                    }
                    ]
                });
                return confirm;
            },
            showAlert: function (title, template) {
                var alertPopup = $ionicPopup.alert({
                    title: title,
                    template: template
                });
                return alertPopup;
            }
        };
    }])
    .factory('$stringResource', ["$http", "$q", function ($http, $q) {
        var allstrings = [];
        $http.get("data/string-resource.json").then(function (res) {
            allstrings = res.data;
        });
        return {
            getValue: function (k) {
                var finValue = '';
                angular.forEach(allstrings, function (val, index) {
                    if (val.Key === k) {
                        finValue = val.Value;
                    }
                });
                return finValue;
            },
            getAll: function () {
                return $http.get("data/string-resource.json");
            }
        }
    }])
    .factory('$config', function () {
        return {
            //IP_PORT: "192.168.1.35:8080",
            IP_PORT: "eepls.tk:8080",
            //IP_PORT: "ablrz.com:8080",
            //IP_PORT: !localStorage['host'] ? "eepls.com:8080" : localStorage['host'],
            APP_VERSION : "1.0.0",
            CONSUMER_ID: localStorage['consumerId'],
            getCustomerId: function () {
                return localStorage['consumerId'];
            }
        }
        //IP_PORT: "rms-api.cfapps.io"
    })
    .factory('$Location', ["$cordovaGeolocation", function ($cordovaGeolocation) {

        var pos = {
            lat: '',
            long: ''
        };
        return {
            getCurrentLocation: function () {
                var posOptions = {
                    enableHighAccuracy: true,
                    timeout: 20000,
                    maximumAge: 0
                };
                console.log($cordovaGeolocation);
                $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
                    pos.lat = position.coords.latitude;
                    pos.long = position.coords.longitude;
                }, function (err) {
                    $popupService.showAlert("Geo Fail", JSON.stringify(err));
                    console.log("position fail");
                    console.log(err);
                });
                return pos;
            }
        }
    }])
    .filter('searchKeyfilter', function ($sce) {
        return function (input, key) {
            var r = RegExp('(' + key + ')', 'gi');
            return input.replace(r, '<b>$&</b>');
        }
    })
    //.factory('PersistentObject', function () {
    //    var obj;


    //})
    ;
})();
