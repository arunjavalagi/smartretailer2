(function () {
    angular.module("myapp")
    .filter('CamelCase', function () {
        return function (text) {
            var reg = /([^\W_]+[^\s-]*) */g; //: /([^\W_]+[^\s-]*)/;
            return text.replace(reg, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); 
            });
        }
    })
    .filter('GetObject', function () {
        return function (propertyName, propertyValue, collection) {
            var result = null;
            angular.forEach(collection, function (value, index) {
                if (value[propertyName] == propertyValue) {
                    result = {
                        'object':value,
                        'index': index
                        };
                }
            })
            return result;
        }
    });
})();