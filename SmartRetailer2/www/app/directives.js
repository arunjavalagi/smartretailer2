﻿(function () {
    angular.module("myapp")
    .directive('fallbackSrc', function () {
        return {
            link: function (scope,elem, attr) {
                elem.bind('error', function () {
                    angular.element(this).attr("src", attr.fallbackSrc);
                });
            }
        }
    });
})();