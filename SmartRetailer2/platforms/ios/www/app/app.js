﻿/// <reference path="templates/view-product-detail.html" />
(function () {
    "use strict";

    angular.module("myapp", ["ionic", "myapp.controllers", "myapp.services", 'tabSlideBox'])
        .run(function ($ionicPlatform, $rootScope) {
            $ionicPlatform.ready(function () {
                if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                }
                if (window.StatusBar) {
                    StatusBar.styleDefault();
                }
                //   AppRate.promptForRating(true);
            });
            $rootScope.SERVER = "eepls.com:8080";
            //$rootScope.SERVER = !localStorage['host'] ? "eepls.com:8080" : localStorage['host'];
        })
        .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

            var idUserLogedIn = localStorage['isUserLogedIn'];
            var isDefaultRetailerSet = localStorage['defaultRetailer'];

            if (idUserLogedIn == null) {
                console.warn("user not loged in");
                $urlRouterProvider.otherwise("/login");
            }
            else if (isDefaultRetailerSet == null) {
                console.warn("default retailer not set");
                $urlRouterProvider.otherwise("/register");
            }
            else {
                console.info('everything set');
                $urlRouterProvider.otherwise("/app/products");
            }
            //$urlRouterProvider.otherwise("/retailersfirst");

            $stateProvider
            .state("app", {
                url: "/app",
                abstract: true,
                templateUrl: "app/templates/view-menu.html",
                controller: "appCtrl"
            })
            .state('app.changeLocation', {
                url: "/changeLocation",
                controller: "changeLocationCtrl",
                templateUrl: "app/templates/view-change-my-location.html"
            })
            .state("initial", {
                url: "/initial",
                templateUrl: "app/templates/view-initial.html",
                controller: "initialCtrl"
            })
            .state("versioncheck", {
                url: "/versioncheck",
                templateUrl: "app/templates/view-versioncheck.html",
                controller: "versioncheckCtrl"
            })
                .state("termsInitial", {
                    url: '/termsInitial',
                templateUrl:'app/templates/view-terms.html'
            })
            .state("register", {
                url: "/register",
                params: { retailerID: '', phoneNo: '' },
                templateUrl: "app/templates/view-register.html",
                controller: "registerCtrl"
            })
            .state("login", {
                url: "/login",
                templateUrl: "app/templates/view-login.html",
                controller: "loginCtrl"
            }).state("app.retailers", {
                url: "/retailers",
                templateUrl: "app/templates/view-retailers.html",
                controller: "retailersCtrl"
            })
            .state('retailersfirst', {
                url: '/retailersfirst',
                templateUrl: 'app/templates/view-retailers-first.html',
                controller: 'retailersfirstCtrl',
                params: { phoneNo: '' }
            })
            .state("home", {
                url: "/home",
                templateUrl: "app/templates/view-home.html",
                controller: "homeCtrl"
            }).state("app.products", {
                url: "/products",
                templateUrl: "app/templates/view-products.html",
                controller: "productsCtrl"

            }).state("app.productdetail", {
                url: "/productdetail",
                params: { product: '' },
                templateUrl: "app/templates/view-product-detail.html",
                controller: "productDetailCtrl"
            }).state("app.productsuggestion", {
                url: "/productsuggestion",
                templateUrl: "app/templates/view-productsuggestion.html",
                controller: "productsuggestionCtrl"
            })
            .state('app.category', {
                url: '/category',
                templateUrl: 'app/templates/view-category.html',
                controller: 'categoryCtrl'
            })
            .state('app.subsegment', {
                url: '/subsegment',
                params: {
                    categoryid:'',
                    segmentid:''
                },
                templateUrl: "app/templates/view-subsegment.html",
                controller: 'subsegmentCtrl'
            })
            .state("app.productcategory", {
                url: "/productcategory",
                params: {
                    id1: '',
                    id2: '',
                    id3: '',
                    subSegName: ''
                },
                templateUrl: "app/templates/view-product-category.html",
                controller: "productcategoryCtrl"
            }).state("app.profile", {
                url: "/profile",
                templateUrl: "app/templates/view-viewprofile.html",
                controller: "viewprofileCtrl"
            }).state("app.orders", {
                url: "/orders",
                templateUrl: "app/templates/view-orders.html",
                controller: "ordersCtrl"
            }).state('app.orderDetail', {
                url: '/orderDetail',
                controller: 'orderDetailCtrl',
                params: {
                    orderID: ''
                },
                templateUrl: "app/templates/view-order-detail.html"
            }).state("app.settings", {
                url: "/settings",
                templateUrl: "app/templates/view-settings.html",
                controller: "settingsCtrl"
            }).state("app.developer", {
                url: "/developer",
                templateUrl: "app/templates/view-developer.html",
                controller: "developerCtrl"
            }).state("app.feedback", {
                url: "/feedback",
                templateUrl: "app/templates/view-feedback.html",
                controller: "feedbackCtrl"
            })
            .state("app.terms", {
                url: '/terms',
                templateUrl:'app/templates/view-terms.html'
            })
            .state("app.contactus", {
                url: "/contactus",
                templateUrl: "app/templates/view-contactus.html",
                controller: "contactUsCtrl"
            })
            .state("app.addToCart", {
                url: "/addToCart",
                templateUrl: "app/templates/view-addtocart.html",
                controller: "addToCartCtrl"
            })
            .state("app.bannerdetails", {
                url: "/bannerdetails",
                templateUrl: "app/templates/view-banner-detail.html",
                controller: "bannerDetailsCtrl",
                params: {
                    bannerID: 'default',
                },
            })
            .state("app.editprofile", {
                url: "/editprofile",
                templateUrl: "app/templates/view-editprofile.html",
                controller: "editprofileCtrl"
            })
            .state("uploadProfileImage", {
                url: "uploadProfileImage",
                templateUrl: "app/templates/view-image-update.html",
                controller: "updateProfileImageCtrl"
            })
            .state("app.reminder", {
                url: '/reminder',
                controller: 'reminderCtrl',
                templateUrl: "app/templates/view-reminder.html"
            })
            .state("app.wishlist", {
                url: '/wishlist',
                controller: 'wishlistCtrl',
                templateUrl: "app/templates/view-wishlist.html"
            })
            .state("app.productSearch", {
                url: '/productSearch',
                controller: 'productSearchCtrl',
                templateUrl: "app/templates/view-productsearch.html"
            })
            .state("app.nondailyneeds", {
                url: '/nondailyneeds',
                controller: 'nondailyNeedsCtrl',
                templateUrl: "app/templates/view-nondailyneeds.html"
            })
            .state("app.dailyneeds", {
                url: '/dailyneeds',
                controller: 'dailyNeedsCtrl',
                templateUrl: "app/templates/view-dailyneeds.html"
            })
            .state('app.retailerprofile', {
                url: '/retailerprofile',
                controller: 'retailerProfile',
                templateUrl:'app/templates/view-retailerprofile.html'
            })
            .state("app.template", {
                url: '/template',
                controller: 'templateCtrl',
                templateUrl: 'app/templates/view-template.html'
            })
            .state("app.templateDetail", {
                url: '/templateDetail',
                controller: 'templateDetailCtrl',
                templateUrl: 'app/templates/view-templatedetail.html',
                params: { template: '' }
            })
            ;
        });

})();

