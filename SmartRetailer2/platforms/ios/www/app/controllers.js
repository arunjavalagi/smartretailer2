﻿(function () {
    "use strict";

    angular.module("myapp.controllers", ['myapp.utils', 'ionic', 'ngCordova', 'ionic-toast', 'tabSlideBox'])

    .controller("appCtrl", ["$scope", "$customlocalstorage", "$ionicPopover", "$rootScope", "$http", "$state", "$filter", "$cordovaGeolocation", "$popupService", "ionicToast", "$config", "$Location", "$ionicLoading", "$ionicHistory", function ($scope, $customlocalstorage, $ionicPopover, $rootScope, $http, $state, $filter, $cordovaGeolocation, $popupService, ionicToast, $config, $Location, $ionicLoading, $ionicHistory) {
        $rootScope.defaultRetailer = $customlocalstorage.getObject('defaultRetailer').storename;
        $scope.category = [];

        // Check whether the token is updated or not (and update)
        if (localStorage['tokenUpdated'] === undefined) {
            $http.put('http://' + $config.IP_PORT + '/consumer/updateToken',
                {
                    "token": localStorage['token'],
                    "id": localStorage['consumerId']
                }).then(function (res) {
                    if (res.data.status === "1") {
                        console.log(res);
                        console.log('token updated');
                        localStorage['tokenUpdated'] = 'done';
                    }
                    else {
                        console.log(res);
                    }
                }, httpError);
        }
        // Load the category tree
        $http.get('http://' + $config.IP_PORT + '/category/retailer/' + $customlocalstorage.getObjectorDefault('defaultRetailer', '{}').id).then(function (res) { $scope.category = res.data; }, httpError);
        // Set the Message
        $http.get('http://' + $config.IP_PORT + '/message/').then(function (res) { $scope.messageToShow = res.data; }); 

        $rootScope.retailerImageUrl = 'http://' + $config.IP_PORT + '/retailer/logo/' + localStorage['retailerID'];

        $scope.categorySelection = {
            category: '',
            segment: '',
            subSegment: ''
        }
        $scope.shownSeg = null;
        $scope.parentProducts = {
            products: []
        };
        $scope.parentObj = {
            cartCount: $customlocalstorage.getObjectorDefault('cartlist', '[]').length,
            prods: ["sdfdsfsd", "dfds"],
            selectedProduct: {}
        };
        $scope.getProducts = function (paramId1, paramId2, paramId3, paramSubSegName) {
            $ionicHistory.nextViewOptions({
                disableBack: true,
                historyRoot: false
            });
            $state.go('app.productcategory', {
                id1: paramId1,
                id2: paramId2,
                id3: paramId3,
                subSegName: paramSubSegName
            });
        }
        $scope.toggleGroup = function (group) {
            $scope.shownGroup = $scope.isGroupShown(group) ? null : group;
        };
        $scope.isGroupShown = function (group) {
            return $scope.shownGroup === group ? true : false;
        };
        $scope.toggleSeg = function (group) {
            $scope.shownSeg = $scope.isSegShown(group) ? null : group;
        };
        $scope.isSegShown = function (group) {
            return $scope.shownSeg === group ? true : false;
        };
        $scope.onProductClick = function (item) {
            $ionicHistory.nextViewOptions({
                disableBack: false,
                historyRoot: false
            });
            $state.go("app.productdetail", { product: item });
        };
        $ionicPopover.fromTemplateUrl('my-popover.html', {
            scope: $scope
        }).then(function (popover) {
            $scope.popover = popover;
        });
        $scope.openPopover = function ($event) {
            $scope.popover.show($event);
            console.log("openPo-pover");
        };
        $scope.closePopover = function () {
            $scope.popover.hide();
            console.log("clo-sePopover");
        };
        $scope.$on('$destroy', function () {
            $scope.popover.remove();
            console.log("$destroy");
        });
        $scope.$on('popover.hidden', function () {
            // Execute action
            console.log("hidden");
        });
        $scope.$on('popover.removed', function () {
            // Execute action
            console.log("removed");
        });
        $scope.getsmiley = function () {
            if (localStorage['lastOrderID']) {
                var reqObj = {
                    date: $filter('date')(new Date(), 'yyyy-MM-dd hh:mm:ss'),
                    customerId: $config.getCustomerId(),
                    orderId: localStorage['lastOrderID'] | '',
                    rate: 3,
                    uuid: device.uuid,
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded;' }
                };
                $http.put('http://' + $config.IP_PORT + '/satisfaction/add/' + reqObj.date + '/' + reqObj.rate + '/' + reqObj.customerId + '/' + reqObj.orderId)
                .success(function () {
                    ionicToast.show('Thank you for liking us!', 'middle', false, 1000);
                })
            }
            else {
                ionicToast.show('You have not ordered anything yet. Submit atleast one order first', 'middle', false, 4000);
            }
        }

        $scope.qtyChanged = function (item, increment) {

            var temp = (item.qty + increment).toString();
            item.qty = 0;
            item.qty = Number(temp);

            if (item.qty == 0) {
                var cartList = $customlocalstorage.getObjectorDefault('cartlist', '[]');
                angular.forEach(cartList, function (value, index) {
                    if (value.productId === item.id) {
                        cartList.splice(index, 1);
                    }
                });
                $customlocalstorage.setObject('cartlist', cartList);
            }
            else if (item.qty >= 0 && item.qty < 1000) {
                var found = false;
                var cartList = $customlocalstorage.getObjectorDefault('cartlist', '[]');
                angular.forEach(cartList, function (value, index) {
                    if (value.productId === item.id) {
                        found = true;
                        cartList[index].Qty = item.qty;
                    }

                });

                if (!found) {
                    cartList.push({ productId: item.id, Qty: item.qty });
                }
                $customlocalstorage.setObject('cartlist', cartList);
            }
            else if (item.qty < 0) {
                item.qty = 0;
            }
            else {
                item.qty = item.qty.substr(1,3);
            }

        };
        $scope.returnQty = function (item) {
            var cartList = $customlocalstorage.getObjectorDefault('cartlist', '[]');
            item.qty = 0;
            angular.forEach(cartList, function (value, index) {
                if (value.productId == item.id) {
                    item.qty = value.Qty;
                }
            });
            return Number(item.qty);
        };
        $scope.isFavourite = function (itemid) {
            var wishlist = $customlocalstorage.getObjectorDefault('wishlist', '[]');
            var obj = $filter('GetObject')('id', itemid, wishlist);
            if (obj == null)
                return false;
            else
                return true;
            var found = false;
        }
        $scope.toggleFavourite = function (item) {
            var wishlist = $customlocalstorage.getObjectorDefault('wishlist', '[]');
            var obj = $filter('GetObject')('id', item.id, wishlist);
            if (obj == null) {
                var tempItem = JSON.parse(JSON.stringify(item));                
                if (!item.qty || item.qty == 0) {
                    tempItem.qty = 1;
                }
                wishlist.push(tempItem);
                ionicToast.show('<b>' + item.name + '</b> added to Remainder list', 'bottom', false, 2000);
            }
            else {
                wishlist.splice(obj.index, 1);
                ionicToast.show('<b>' + item.name + '</b> removed from Remainder list', 'bottom', false, 2000);
            }
            $customlocalstorage.setObject('wishlist', wishlist);
        }
        $scope.getCartListCount = function () {
            return $customlocalstorage.getObjectorDefault('cartlist', '[]').length;
        };
        $scope.exitApp = function () {
            ionic.Platform.exitApp();
        };
        $scope.goHome = function () {
            $ionicHistory.nextViewOptions({
                disableBack: true,
                historyRoot: true
            });
            $state.go('app.products');
        }

    }])

    .controller("changeLocationCtrl", ["$scope", "$customlocalstorage", "$state", "$popupService", "$stringResource", "$compile", "$ionicLoading", "ionicToast", "$http", "$config", "$cordovaGeolocation", function ($scope, $customlocalstorage, $state, $popupService, $stringResource, $compile, $ionicLoading, ionicToast, $http, $config, $cordovaGeolocation) {
        PageButtonClicked("Change Location");
        $scope.profile = $customlocalstorage.getObject('consumer');

        $scope.map = new google.maps.Map(document.getElementById("map"), {
            center: new google.maps.LatLng($scope.profile.latitude, $scope.profile.longitude),
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        $scope.marker = new google.maps.Marker({
            map: $scope.map,
            animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng($scope.profile.latitude, $scope.profile.longitude),
            draggable: true
        });
        $scope.marker._lat = $scope.marker.position.lat();
        $scope.marker._lng = $scope.marker.position.lng();

        google.maps.event.addListener($scope.marker, 'drag', function () {
            $scope.marker._lat = $scope.marker.position.lat();
            $scope.marker._lng = $scope.marker.position.lng();
            $scope.$apply();
        });
        google.maps.event.addListenerOnce($scope.map, 'idle', function () {
            google.maps.event.addListener($scope.marker, 'click', function () {
                new google.maps.InfoWindow({
                    content: "Selected location"
                }).open($scope.map, $scope.marker);
            });
        });

        var getCurrectLocation = function () {
            $ionicLoading.show({
                template: 'Getting current location...',
                showBackdrop: true
            });
            var options = { timeout: 5000, enableHighAccuracy: true };
            $cordovaGeolocation.getCurrentPosition(options).then(function (position) {
                var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                $scope.map.setCenter(latLng);
                $scope.marker.setPosition(latLng);
                $scope.marker._lat = $scope.marker.position.lat();
                $scope.marker._lng = $scope.marker.position.lng();
                $ionicLoading.hide();
            }, function (error) {
                navigator.notification.alert('Error getting location...', null, 'Location Error', Ok);
                $ionicLoading.hide();
            });
        };
        getCurrectLocation();
        $scope.showSavedLocation = function () {
            var latLng = new google.maps.LatLng($scope.profile.latitude, $scope.profile.longitude);
            $scope.map.setCenter(latLng);
            $scope.marker.setPosition(latLng);
            $scope.marker._lat = $scope.marker.position.lat();
            $scope.marker._lng = $scope.marker.position.lng();
        }
        $scope.showCurrentLocation = function () {
            getCurrectLocation();
        };
        $scope.saveLocation = function () {
            $scope.profile.latitude = $scope.marker.position.lat();
            $scope.profile.longitude = $scope.marker.position.lng();
            $customlocalstorage.setObject('consumer', $scope.profile);

            var updateProfileData = {
                id: Number(localStorage['consumerId']),
                "latitude": $scope.marker.position.lat(),
                "longitude": $scope.marker.position.lng(),
            }
            $ionicLoading.show({
                template: "Updating your location..."
            });
            $http.post('http://' + $config.IP_PORT + '/consumer/Update', JSON.stringify(updateProfileData)).then(function (res) {
                if (res.data.status == "1") {
                    ionicToast.show('Location updated', 'bottom', false, 3000);
                }
                else {
                    ionicToast.show('Location updated failed', 'bottom', false, 3000);
                }
                $ionicLoading.hide();
            }, function (err) {
                ionicToast.show("Error updating location, check connectivity.", 'bottom', false, 3000);
                $ionicLoading.hide();
            });
        }
    }])

    .controller("initialCtrl", ["$scope", "$customlocalstorage", "$state", "$popupService", "$stringResource", function ($scope, $customlocalstorage, $state, $popupService, $stringResource) {
        PageButtonClicked("Initial");
    }])

    .controller("registerCtrl", ["$scope", "$state", "$http", "$popupService", "$stateParams", "$filter", "$config", "$cordovaGeolocation", "$customlocalstorage", "$ionicLoading", "$cordovaDatePicker", "ionicToast",
    function ($scope, $state, $http, $popupService, $stateParams, $filter, $config, $cordovaGeolocation, $customlocalstorage, $ionicLoading, $cordovaDatePicker, ionicToast) {
        PageButtonClicked("Consumer Registration");

        $scope.form = {
            mobileNo: $stateParams.phoneNo,
            //dateOfBirth: $filter('date')(new Date(), 'dd-MM-yyyy')
            //dateOfBirth: $filter('date') 
            city: 'Bangalore',
            state: 'Karnataka'
        };
        $scope.isVisible = false;
        $scope.getDate = function () {
            var options = {
                date: new Date(),
                mode: 'date', // or 'time'
                minDate: new Date(),
                maxDate: new Date(),
                allowOldDates: false,
                allowFutureDates: false,
                doneButtonLabel: 'DONE',
                doneButtonColor: '#F2F3F4',
                cancelButtonLabel: 'CANCEL',
                cancelButtonColor: '#000000'
            };
            $cordovaDatePicker.show(options).then(function (date) {
                console.log(date);
                $scope.form.dateOfBirth = $filter('date')(date, 'dd-MM-yyyy');
            }, function (err) {
                console.log(err);
                $popupService.showAlert('Date picker error', JSON.stringify(err));
            });
        };
        $scope.getGender = function () {

        }
        $scope.retailerID = $stateParams.retailerID;
        $scope.textChange = function (text) {
            return $filter('CamelCase')(text);
        };
        $scope.validateForm = function () {
            var popupContent = '';

            if (angular.isUndefined($scope.form.name) || !$scope.form.name.toString().match(/^[A-z\s]{3,}$/)) {
                popupContent = popupContent.concat("Name must have correct value");
            }
            else if (angular.isUndefined($scope.form.email) || !$scope.form.email.match(/^[\w|\d|\._]{4,}@[\w|\d|\._]{2,}\.[\w|\d|\._]+$/g)) {
                popupContent = popupContent.concat("Enter the correct email address");
            }
            else if (angular.isUndefined($scope.form.dateOfBirth) || !$scope.form.dateOfBirth.match(/^\d{1,2}-\d{1,2}-\d{4}$/)) {
                popupContent = popupContent.concat("Enter the correct DOB");
            }
            else if (angular.isUndefined($scope.form.street) || $scope.form.street.length < 3) {
                popupContent = popupContent.concat("Enter the correct street name");
            }
            else if (!!!$scope.form.zipcode || !$scope.form.zipcode.toString().match(/^\d{6}$/)) {
                popupContent = popupContent.concat("Enter the correct zipcode");
            }

            if (popupContent !== '') {
                $popupService.showAlert('Validation Error<i class="icon item-icon-left"></i>', popupContent);
            }
            else {
                return true;
            }
            return false;
        }
        $scope.register = function (form) {
            if ($scope.validateForm()) {
                $ionicLoading.show({
                    template: 'Registration in progress...',
                    duration: 10000
                });
                var reqObj =
                    {
                        mobileNo: $scope.form.mobileNo,
                        deviceId: device.uuid,
                        name: $scope.form.name,
                        dateOfBirth: $filter('date')(new Date($scope.form.dateOfBirth.substring(6, 10), parseInt($scope.form.dateOfBirth.substring(3, 5)) - 1, parseInt($scope.form.dateOfBirth.substring(0, 2)) + 1), 'yyyy-MM-dd'),
                        latitude: "",
                        longitude: "",
                        gender: $scope.form.gender,
                        mailId: $scope.form.email,
                        retailer: {
                            id: $scope.retailerID
                        },
                        residenceaddress: {
                            zipCode: $scope.form.zipcode,
                            street: $scope.form.street,
                            city: {
                                id: 1
                            },
                            state: {
                                id: 17
                            }
                        },
                        preference: {
                            deliveryTime: $filter('date')(new Date(), 'yyyy-MM-dd'),
                            paymentMode: "cash"
                        },
                        consumerPhone: [{
                            contactType: "PRIMARY",
                            phoneNumber: $scope.form.mobileNo,
                            type: "MOBILE"
                        }]
                    };
                var registerNow = function () {
                    $ionicLoading.show({
                        template: 'Registration in progress...',
                        duration: 10000
                    });

                    var req = {
                        method: 'POST',
                        url: 'http://' + $config.IP_PORT + '/consumer/create',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        data: JSON.stringify(reqObj)
                    }
                    $http(req).then(function (res) {
                        $ionicLoading.hide();
                        console.log(res.data);
                        if (res.data.status == '1') {
                            $customlocalstorage.setObject('registration', res.data.id);
                            $customlocalstorage.set('isUserLogedIn', 'true');
                            $http.get('http://' + $config.IP_PORT + '/consumer/id/' + res.data.id).then(function (res) {
                                $customlocalstorage.setObject('consumer', res.data);
                            }, function (err) {
                                ionicToast.show('Unable to get consumer details after registration.', 'bottom', false, 3000);
                            });
                            localStorage['consumerId'] = res.data.id;
                            ionicToast.show('Registration success', 'bottom', false, 2000);
                            //$state.go("uploadProfileImage");

                            $popupService.showAlert('Success', 'Registration Successful, Please log in')
                            .then(function () {
                                localStorage.clear();
                                document.location.href = 'index.html';
                            });



                        }
                        else {
                            $popupService.showAlert('Failed', res.data.message);
                        }
                    }, function (err) {
                        console.warn(err);
                        $ionicLoading.hide();
                        $popupService.showAlert('Failed', 'Registration failed' + JSON.stringify(err));
                    });
                };
                var options = {
                  enableHighAccuracy: true,
                  timeout: 5000,
                  maximumAge: 0
                };
                navigator.geolocation.getCurrentPosition(function (pos) {
                    reqObj.latitude = pos.coords.latitude;
                    reqObj.longitude = pos.coords.longitude;
                    console.log(reqObj);
                    registerNow();
                }, function (err) {
                    console.log(reqObj);
                    ionicToast.show('Unable to get the geo location.', 'middle', false, 2000);
                    registerNow();
                }, options);
            }
        };
    }])

    .controller('versioncheckCtrl', ["$scope", "$stateParams", "$state", "$http", "$config", function ($scope, $stateParams, $state, $http, $config) {

        $http.get('http://' + $config.IP_PORT + '/consumer/appversion').then(function (res) {
            var appDownloadURL = res.data.downloadUrl;

            $scope.downloadLatestVersion = function () {
                var fileTransfer = new FileTransfer();
                fileTransfer.download(encodeURI(appDownloadURL),
                    "cdvfile://localhost/temporary/Consumer.apk",
                    function (entry) {
                        window.plugins.webintent.startActivity({
                            action: window.plugins.webintent.ACTION_VIEW,
                            url: entry.toURL(),
                            type: 'application/vnd.android.package-archive'
                        }, function () {
                        }, function () {
                            alert('Failed to open URL via Android Intent.');
                            console.log("Failed to open URL via Android Intent. URL: " + entry.fullPath);
                        });
                    }, function (error) {
                        console.log("download error source " + error.source);
                        console.log("download error target " + error.target);
                        console.log("upload error code" + error.code);
                    }, true);
               // location.reload();
            };

        });

    }])

    .controller("loginCtrl", ["$scope", "$state", "$customlocalstorage", "$http", "$popupService", "$stringResource", "$config", "$ionicLoading", "ionicToast", "$ionicHistory", function ($scope, $state, $customlocalstorage, $http, $popupService, $stringResource, $config, $ionicLoading, ionicToast, $ionicHistory) {
        var latestAppVersion = '';
            $http.get('http://' + $config.IP_PORT + '/consumer/appversion').then(function (res) {
                latestAppVersion = res.data.appversion;
                       
            if (latestAppVersion != $config.APP_VERSION && latestAppVersion != null && latestAppVersion != '') {
            $state.go("versioncheck");
        }
        else {
            $scope.data = {};
            var validatePhone = function () {
                if (!$scope.data.phone || $scope.data.phone.toString().length != 10 || $scope.data.phone <= 6999999999) {
                    ionicToast.show('Phone number is not valid', 'bottom', false, 3000);
                    return false;
                }
                return true;
            };
            $scope.proceed = function () {
                $ionicLoading.show({
                    template: 'Processing...',
                    duration: 10000
                });
                if (validatePhone()) {
                    $scope.disable = true;

                    $http.post('http://' + $config.IP_PORT + '/user/login', {
                        mobileNo: $scope.data.phone,
                        uuid: device.uuid,
                        password: $scope.data.password
                    }).then(function (res) {
                        $ionicLoading.hide();
                        console.log(res.data);
                        if (res.data.status === "1") {
                            $customlocalstorage.setObject('defaultRetailer', res.data.retailer);
                            $popupService.showAlert('Success', 'Login success! The retailer <b>' + res.data.retailer.storename + '</b> has been set as default.')
                                .then(function () {
                                    localStorage['isUserLogedIn'] = 'true';
                                    $http.get('http://' + $config.IP_PORT + '/consumer/id/' + res.data.user_id).then(function (res) {
                                        $customlocalstorage.setObject('consumer', res.data);
                                        //res.data.address
                                        //ShippingAddress
                                        $customlocalstorage.setObject('ShippingAddress', res.data.address);
                                        $customlocalstorage.setObject('confirmAddressToStorage', 'false')
                                        ;
                                    }, function (err) {
                                        ionicToast.show('Unable to get consumer details after registration.', 'bottom', false, 3000);
                                    });
                                    localStorage['consumerId'] = res.data.user_id;
                                    $state.go('app.products');
                                });
                        }
                        else if (res.data.status === "0") {
                            $http.get('http://' + $config.IP_PORT + '/invitation/' + $scope.data.phone).then(function (res) {
                                console.log(res.data);
                                if (res.data.status === "1") {
                                    $popupService.showAlert('Invitation', 'You have got an Invitation from the retailer <b>' + res.data.invitation.retailer.storename + '</b>. The retailer will be set as a Default retailer.');
                                    $customlocalstorage.setObject('defaultRetailer', res.data.invitation.retailer);
                                    $state.go('register', { retailerID: res.data.invitation.retailer.id, phoneNo: $scope.data.phone });
                                }
                                else {
                                    $popupService.showAlert('New User', 'Your number is new to us. Please select the default retailer and proceed for Registration');
                                    $state.go('app.retailersfirst', { phoneNo: $scope.data.phone });
                                }
                            });
                        }
                        else {
                            $popupService.showAlert('Login Failed', '');
                        }
                    }, function (err) {
                        $ionicLoading.hide();
                        $popupService.showAlert('Network Error', JSON.stringify(err));
                    });
                }
                else {
                    $ionicLoading.hide();
                }
            };
            $scope.register = function () {
                if (!$scope.data.phone || $scope.data.phone.toString().length != 10 || $scope.data.phone <= 6999999999) {
                    ionicToast.show('Please enter the valid phone number to register.', 'bottom', false, 3000);
                    return false;
                }
                else {
                    $ionicLoading.show({
                        template: 'Checking whether you got an invitation or not...',
                        duration: 10000
                    });
                    $http.get('http://' + $config.IP_PORT + '/invitation/' + $scope.data.phone).then(function (res) {
                        $ionicLoading.hide();
                        console.log(res.data);
                        if (res.data.status === "1") {
                            $popupService.showAlert('Invitation', 'You have got an Invitation from the retailer <b>' + res.data.invitation.retailer.storename + '</b>. The retailer will be set as a Default retailer.');
                            $customlocalstorage.setObject('defaultRetailer', res.data.invitation.retailer);
                            $state.go('register', { retailerID: res.data.invitation.retailer.id, phoneNo: $scope.data.phone });
                        }
                        else {
                            $state.go('retailersfirst', { phoneNo: $scope.data.phone });
                        }
                    });
                }
            };
            $scope.forgotPassword = function () {
                $popupService.showConfirm("Forgot password", "The new password will be sent to your phone via SMS.", $scope.data)
                .then(function () {
                    if ($scope.data.confirm) {
                        if (!$scope.data.phone || $scope.data.phone.toString().length != 10 || $scope.data.phone <= 6999999999) {
                            ionicToast.show('Please enter the valid phone number to register.', 'bottom', false, 3000);
                            return false;
                        }
                        else {
                            $http.post('http://' + $config.IP_PORT + '/user/generatePassword/consumer', { mobileNo: $scope.data.phone }).then(function (res) {
                                ionicToast.show(res.data.message, 'bottom', false, 3000);
                            });
                        }
                    }
                });
            };
            }
    });
    }])

    .controller('bannerDetailsCtrl', ["$scope", "$stateParams", "$state", function ($scope, $stateParams, $state) {
        PageButtonClicked("Banner Details");
        $scope.id = $stateParams.bannerID;
        // console.log($stateParams.bannerID);
    }])

    .controller("retailersCtrl", ["$scope", "$state", "$customlocalstorage", "$http", '$rootScope', '$config', 'ionicToast', '$stateParams', '$filter', '$timeout', function ($scope, $state, $customlocalstorage, $http, $rootScope, $config, ionicToast, $stateParams, $filter, $timeout) {
        PageButtonClicked("Change Retailer");
        $scope.data = {
            searchkey: '',
            suggestions: [],
            isSuggestionShown: false,
            isSearchResultShown: true,
            retailers: []
        };
        var lat = $customlocalstorage.getObjectorDefault('consumer', '{}').latitude;
        var long = $customlocalstorage.getObjectorDefault('consumer', '{}').longitude;
        $http.get('http://' + $config.IP_PORT + '/retailer/retailersAroundLatLong/' + lat + '/' + long + '/2').then(function (res) {
            $scope.data.retailers = res.data;
        }, function (err) {
            ionicToast.show('Unable to get nearby retailers.', 'bottom', false, 2000);
        });

        $scope.data.suggestions = [];
        $scope.retailer = $customlocalstorage.getObject('defaultRetailer').storename;
        $scope.data.choice = '';
        console.log($scope.phoneNo);
        $scope.refresh = function () {
            //refresh binding
            $scope.$broadcast("scroll.refreshComplete");
        };
        $scope.search = function (header, value) {
            $scope.data.isSearchResultShown = true;
            $scope.data.isSuggestionShown = false;
            var url = '';
            if (header === 'StoreName') {
                url = 'http://' + $config.IP_PORT + '/retailer/storename/' + value;
            } else if (header === 'Area') {
                url = 'http://' + $config.IP_PORT + '/retailer/area/' + value;
            } else if (header === 'Pincode') {
                url = 'http://' + $config.IP_PORT + '/retailer/pincode/' + value;
            }

            $http.get(url)
               .then(function (res) {
                   $scope.data.retailers = res.data;
                   $scope.getStyle = function (status) {
                       {
                           if ($scope.data.retailers[0].status === "HOLD") {

                               return "GREY";
                           }
                       }
                   }
               }, function () { });

        };
        var getSuggRegDone = false;
        $scope.searchSuggestion = function () {
            $scope.data.isSuggestionShown = true;
            $scope.data.isSearchResultShown = false;

            if ($scope.data.searchkey === '') {
                $scope.data.isSuggestionShown = false;
                $scope.data.isSearchResultShown = true;
                var lat = $customlocalstorage.getObject('consumer').latitude;
                var long = $customlocalstorage.getObject('consumer').longitude;
                $http.get('http://' + $config.IP_PORT + '/retailer/retailersAroundLatLong/' + lat + '/' + long + '/2').then(function (res) {
                    $scope.data.retailers = res.data;
                }, function (err) {
                    ionicToast.show('Unable to get nearby retailers.', 'bottom', false, 2000);
                });

                $scope.data.suggestions = [];
            }
            else if (getSuggRegDone === false) {
                getSuggRegDone = true;
                $timeout(function () {
                    getSuggRegDone = false;
                    $http.get('http://' + $config.IP_PORT + '/retailer/suggestion/1/' + $scope.data.searchkey)
                        .then(function (res) {
                            $scope.data.suggestions = [];
                            var defaultRetailer = $customlocalstorage.getObject('defaultRetailer');
                            angular.forEach(res.data, function (obj, objI) {
                                var found = false;
                                angular.forEach(obj, function (value, index) {
                                    switch (index) {
                                        case "Area": found = defaultRetailer.street == value.trim() ? true : false;
                                            break;
                                        case "StoreName": found = defaultRetailer.storename == value.trim() ? true : false;
                                            break;
                                        case "Pincode": found = defaultRetailer.storeaddress.zipCode == value.trim() ? true : false;
                                            break;
                                    }
                                });
                                if (!found) {
                                    $scope.data.suggestions.push(obj);
                                }
                            });
                            console.log(res.data);
                        }, function (err) {
                            console.log(err);
                        });
                }, 300);
            }
        };
        $scope.closePopover = function () {
            $scope.popover.hide();
            console.log("closePopover from retailer");
        };
        $scope.setAsDefaultRetailer = function (retailer) {
            console.log($customlocalstorage.getObject("defaultRetailer"));

            if (retailer.status === "HOLD") {
                ionicToast.show('The ' + retailer.storename + ' has marked as UNAVALIABLE Please choose different Retailer by your location', 'middle', false, 6500)
                $state.go('app.retailers');
            }
            else if (!retailer.isApproved) {
                ionicToast.show('The ' + retailer.storename + ' is not active. Please choose different Retailer', 'middle', false, 4000)
            }
            else if (retailer.id == $customlocalstorage.getObject("defaultRetailer").id) {
                ionicToast.show('The ' + retailer.storename + ' has already marked as default.', 'middle', false, 4000)
            }
            else {
                $http({
                    url: 'http://' + $config.IP_PORT + '/consumer/setDefaultRetailer/' + localStorage['consumerId'] + '/' + $scope.data.choice,
                    method: 'PUT'
                }).then(function (res) {
                    console.log(res);
                    if (res.status === 200) {
                        $customlocalstorage.setObject("defaultRetailer", retailer);
                        $rootScope.defaultRetailer = retailer.storename;
                        ionicToast.show("Your default Retailer has been changed to " + retailer.storename.toUpperCase(), "middle", false, 2500);
                        $scope.retailer = retailer.storename;
                        document.location.href = 'index.html';
                    }
                    else {
                        ionicToast.show("Oops! Default Retailer not set", "middle", false, 2500);
                    }
                }, function (err) {
                    console.log(err);
                    ionicToast.show("Oops! Default Retailer not set", "middle", false, 2500);
                });
            }

        };
    }])

    .controller("retailersfirstCtrl", ["$scope", "$state", "$customlocalstorage", "$http", '$rootScope', '$config', 'ionicToast', '$stateParams', '$filter',
function ($scope, $state, $customlocalstorage, $http, $rootScope, $config, ionicToast, $stateParams, $filter) {
    PageButtonClicked("Change Retailer");
    $scope.data = {
        searchkey: '',
        suggestions: [],
        isSuggestionShown: false,
        isSearchResultShown: false,
        retailers: []
    };
    var options = {
                      enableHighAccuracy: true,
                      timeout: 5000,
                      maximumAge: 0
                    };
    navigator.geolocation.getCurrentPosition(function (pos) {
        registerNow(pos.coords.latitude, pos.coords.longitude);
    }, function (err) {
        ionicToast.show('Unable to get the geo location.', 'middle', false, 2000);
        registerNow(0, 0);
    },options);

    var registerNow = function (lat, long) {
        $http.get('http://' + $config.IP_PORT + '/retailer/retailersAroundLatLong/' + lat + '/' + long + '/2').then(function (res) {
            $scope.data.retailers = res.data;
        }, function (err) {
            ionicToast.show('Unable to get nearby retailers.', 'bottom', false, 2000);
        });
    }
    $scope.data.choice = '';
    $scope.phoneNo = $stateParams.phoneNo;
    console.log($scope.phoneNo);
    $scope.search = function (header, value) {
        $scope.data.isSearchResultShown = true;
        $scope.data.isSuggestionShown = false;
        var url = '';
        if (header === 'StoreName') {
            url = 'http://' + $config.IP_PORT + '/retailer/storename/' + value;
        } else if (header === 'Area') {
            url = 'http://' + $config.IP_PORT + '/retailer/area/' + value;
        } else if (header === 'Pincode') {
            url = 'http://' + $config.IP_PORT + '/retailer/pincode/' + value;
        }

        $http.get(url)
           .then(function (res) {
               $scope.data.retailers = res.data;
           }, function () { });

    };
    $scope.searchSuggestion = function () {
        $scope.data.isSuggestionShown = true;
        $scope.data.isSearchResultShown = false;

        if ($scope.data.searchkey === '') {
            $scope.data.isSuggestionShown = false;
            $scope.data.isSearchResultShown = true;
            var lat = $customlocalstorage.getObject('consumer').latitude;
            var long = $customlocalstorage.getObject('consumer').longitude;
            $http.get('http://' + $config.IP_PORT + '/retailer/retailersAroundLatLong/' + lat + '/' + long + '/2').then(function (res) {
                $scope.data.retailers = res.data;
            }, function (err) {
                ionicToast.show('Unable to get nearby retailers.', 'bottom', false, 2000);
            });

            $scope.data.suggestions = [];
            return;
        }

        $http.get('http://' + $config.IP_PORT + '/retailer/suggestion/1/' + $scope.data.searchkey)
            .then(function (res) {
                $scope.data.suggestions = res.data;
                console.log(res.data);
            }, function (err) {
                console.log(err);
            });

        console.log("suggestion called");

    };
    $scope.setAsDefaultRetailer = function (retailer) {

        if (retailer.status === "HOLD") {
            ionicToast.show('The ' + retailer.storename + ' has marked as UNAVALIABLE Please choose different Retailer by your location', 'middle', false, 6500)
            $state.go('app.retailers');
        }
        else if (!retailer.isApproved) {
            ionicToast.show('The ' + retailer.storename + ' is not active. Please choose different Retailer', 'middle', false, 4000)
        }
        else if (retailer.id == $customlocalstorage.getObject("defaultRetailer").id) {
            ionicToast.show('The ' + retailer.storename + ' has already marked as default.', 'middle', false, 4000)
        }
        else {
            ionicToast.show('The ' + retailer.storename + ' is selected as default', 'bottom', false, 2000)
            $customlocalstorage.setObject('defaultRetailer', retailer);
            $state.go('register', { retailerID: $scope.data.choice, phoneNo: $stateParams.phoneNo });
        }
    };
}])

    .controller("productsCtrl", ["$scope", "$state", "$customlocalstorage", "$http", "$ionicPopover", "$window", "$ionicSlideBoxDelegate", "$popupService", "$stateParams", "$ionicHistory", "$config", function ($scope, $state, $customlocalstorage, $http, $ionicPopover, $window, $ionicSlideBoxDelegate, $popupService, $stateParams, $ionicHistory, $config) {
        var latestAppVersion = '';
        //latestAppVersion = $http.get('http://' + $config.IP_PORT + '/consumer/appversion').value;
        $http.get('http://' + $config.IP_PORT + '/consumer/appversion').then(function (res) {
            latestAppVersion = res.data.appversion;
        
        if (latestAppVersion != $config.APP_VERSION && latestAppVersion != null && latestAppVersion != '') {
            $state.go("versioncheck");
        }
        else {
            $scope.data = { searchkey: '' };
            $scope.bannerImage = '';
            $scope.parentObj.products = [];
            $scope.imageArray = [
                                    { "thumbnailURL": 'http://' + $config.IP_PORT + '/product/image/1/1' },
                                    { "thumbnailURL": 'http://' + $config.IP_PORT + '/product/image/2/1' },
                                    { "thumbnailURL": 'http://' + $config.IP_PORT + '/product/image/3/1' },
                                    { "thumbnailURL": 'http://' + $config.IP_PORT + '/product/image/4/1' },
                                    { "thumbnailURL": 'http://' + $config.IP_PORT + '/product/image/5/1' }
            ];
            $scope.popupData = {
                confirm: false,
                resposneData: ''
            };
            //$scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            //    if (toState.name == "app.products") {
            //        console.log("current title:", $ionicHistory.currentTitle());
            //        $ionicHistory.currentTitle("eepls");
            //        console.log("changed title:", $ionicHistory.currentTitle());
            //    }
            //});
            //check whether the default retailer is approved or not
            $http.get('http://' + $config.IP_PORT + '/retailer/' + $customlocalstorage.getObjectorDefault('defaultRetailer', '{}').id).then(function (res) {
                if (res.data.isApproved != 1) {
                    //navigator.notification.alert('Your default retailer is not approved yet. Concider changing your default retailer or continue to Order to the same retailer.', function () { }, 'Retailer not Approved', 'Got it');
                    $popupService.showAlert("Retailer not Approved", "Your default retailer is not approved yet. Concider changing your default retailer or continue to Order to the same retailer.", "");
                }
                else if (res.data.status != 'ACTIVE') {
                    //navigator.notification.alert('Your default retailer is not Active. Concider changing your default retailer or continue to Order to the same retailer.', function () { }, 'Retailer not Active', 'Got it');
                    $popupService.showAlert("Retailer not Active", "Your default retailer is not Active. Concider changing your default retailer or continue to Order to the same retailer.", "");
                }
            }, httpError);

            $ionicSlideBoxDelegate.slide(0);
            $ionicSlideBoxDelegate.enableSlide(2000);

            setTimeout(function () {
                $ionicSlideBoxDelegate.slide(0);
                $ionicSlideBoxDelegate.update();
                $scope.$apply();
            });

            $scope.showBannerDetails = function (id) {
                $state.go('app.bannerdetails', { bannerID: id.thumbnailURL });
                console.log(id);
            };
            $scope.showPopup = function () {
                var ret = $popupService.showConfirm("Title", "Templete", $scope.popupData);
                ret.then(function (e) {
                    console.log("then");
                    console.log($scope.popupData.confirm);
                });
            };
            $scope.showAlert = function () {

                $http.get("http://www.google.com").then(function (res) {
                    $popupService.showAlert("Success", JSON.stringify(res.data));
                }, function (res) {
                    $popupService.showAlert("Fail", JSON.stringify(res));
                })
                //var ret = $popupService.showAlert("Title", "This is the text from the called method. This the test to make the text <b>BOLD</>.");
            };

            $scope.changeSlide = function (index) {
                console.log(index);
                $ionicSlideBoxDelegate.slide(index);
            };

            $ionicPopover.fromTemplateUrl('my-popover.html', {
                scope: $scope
            }).then(function (popover) {
                $scope.popover = popover;
            });

            $scope.openPopover = function ($event) {
                $scope.popover.show($event);
                console.log("openPopover");
            };
            $scope.$on('$destroy', function () {
                $scope.popover.remove();
                console.log("$destroy");
            });
            $scope.$on('popover.hidden', function () {
                // Execute action
                console.log("hidden");
            });
            $scope.$on('popover.removed', function () {
                // Execute action
                console.log("removed");
            });

            $scope.refresh = function () {
                //refresh binding
                console.log("refresh");
                $scope.$broadcast("scroll.refreshComplete");
            };
        }
    });
    }])

    .controller("productsuggestionCtrl", ["$scope", "$state", "$customlocalstorage", "$http", "ionicToast", '$config', '$filter', "$ionicLoading", "$sce", "$ionicHistory", "$stateParams", "$timeout", function ($scope, $state, $customlocalstorage, $http, ionicToast, $config, $filter, $ionicLoading, $sce, $ionicHistory, $stateParams, $timeout) {
        PageButtonClicked("Product Suggestion");
        $scope.data = {
            searchkey: '',
            cached: true
        };
        $scope.productCount = 0;
        $scope.showSugg = true;
        $scope.showProductList = false;
        $scope.searchList = [];
        $scope.recentSearch = $customlocalstorage.getObjectorDefault('recentSearch', '[]');
        $scope.productDetailList = [];
        var getSuggRegDone = false;
        $scope.searchSuggestion = function () {
            $scope.showSugg = true;
            $scope.showProductList = false;
            if (getSuggRegDone === false) {
                getSuggRegDone = true;
                $timeout(function () {
                    getSuggRegDone = false;
                    if ($scope.data.searchkey === "") {
                        $scope.data.cached = true;
                        $scope.recentSearch = $filter('orderBy')($scope.recentSearch, "hitCount");
                        $scope.searchList = [];
                        angular.forEach($scope.recentSearch, function (value, index) {
                            $scope.searchList.push(value.key);
                        });
                        $scope.searchList = $scope.searchList.splice(0, 5);
                    }
                    else {
                        $scope.data.cached = false;
                        $http.get('http://' + $config.IP_PORT + '/product/suggestion/' + $scope.data.searchkey).then(function (res) {
                            $scope.searchList = res.data;
                        });
                    }
                }, 200);
            }
        };
        $scope.suggestionClick = function (text) {
            $ionicLoading.show({
                template: "Products Loading...",
            });
            $scope.showSugg = false;
            $scope.showProductList = true;
            $scope.productCache = [];
            $http.get('http://' + $config.IP_PORT + '/product/search/' + text).then(function (res) {
                $ionicLoading.hide();
                $scope.productDetailList = res.data.customeProducts;
                angular.forEach($scope.productDetailList, function (val, ind) {
                    $scope.productDetailList[ind].thumbnailUrl = ""
                });
                $scope.productCache = $customlocalstorage.getObjectorDefault('recentSearch', '[]');
                var foundFlag = false;
                angular.forEach($scope.productCache, function (value, index) {
                    if (value.key === text) {
                        foundFlag = true;
                        $scope.productCache[index].hitCount++;
                    }
                });
                if (!foundFlag) {
                    $scope.productCache.push({ key: text, hitCount: 1 });
                }
                $scope.productCache = $filter('orderBy')($scope.productCache, 'hitCount', true);
                $customlocalstorage.setObject("recentSearch", $scope.productCache);
                $ionicLoading.hide();
            }, function () {
                ionicToast.show('Problem loading products.', 'bottom', false, 2000);
                $ionicLoading.hide();
            });
        };
        angular.forEach($scope.recentSearch, function (value, index) {
            $scope.searchList.push(value.key);
        });
        $scope.searchList = $scope.searchList.sort();
    }])

    .controller('productSearchCtrl', ['$scope', '$http', '$state', '$ionicLoading', '$config', '$ionicScrollDelegate', '$customlocalstorage', function ($scope, $http, $state, $ionicLoading, $config, $ionicScrollDelegate, $customlocalstorage) {
        PageButtonClicked("Product Search");
        $scope.data = {
            searchkey: '',
            Products: [],
            suggestions: {
                indsubsegments: [],
                categories: [],
                subsegments: [],
                segments: []
            },
            showSuggestion: false,
            showSearchHistory: true,
            showProducts: false,
            showCategory: false,
            showSegments: false,
            showSubsegments: false,
        };
        $scope.searchHistory = $customlocalstorage.getObjectorDefault('searchHistory', '[]').sort(function (a, b) { return a.date > b.date; });
        var saveSearchHistory = function (key) {
            $scope.searchHistory = $customlocalstorage.getObjectorDefault('searchHistory', '[]');
            $scope.searchHistory.push({
                'key': key,
                'date': new Date()
            });

            $scope.searchHistory = $scope.searchHistory.sort(function (a, b) { return a.date < b.date; });
            $customlocalstorage.setObject('searchHistory', $scope.searchHistory);
        }

        $scope.suggestions = function () {
            if (!!$scope.data.searchkey) {
                $scope.data.showSuggestion = true;
                $scope.data.showProducts = false;
                $scope.data.showSearchHistory = false;
                $http.get('http://' + $config.IP_PORT + '/product/suggestion/' + $scope.data.searchkey + '/retailer/' + $customlocalstorage.getObjectorDefault('defaultRetailer', '{}').id).then(function (res) {
                    $scope.data.suggestions = res.data;
                    $ionicScrollDelegate.scrollTop();
                }, httpError);
            }
            else {
                $scope.data.showSearchHistory = true;
                $scope.data.showSuggestion = false;
                $scope.data.showProducts = false;
            }

        };
        $scope.searchSubsegment = function (subseg) {
            saveSearchHistory($scope.data.searchkey);
            $ionicLoading.show({ template: 'Loading products from ' + subseg.name });
            $scope.data.showSuggestion = false;
            $scope.data.showProducts = true;
            $http.get('http://' + $config.IP_PORT + '/product/subsegment/' + subseg.id + '/searchKey/' + $scope.data.searchkey + '/retailer/' + $customlocalstorage.getObjectorDefault('defaultRetailer', '{}').id).then(function (res) {
                $ionicLoading.hide();
                $scope.data.Products = res.data;
                $scope.data.Products.forEach(function (e) { e.qty = $scope.returnQty(e); });
                $ionicScrollDelegate.scrollTop();
            }, httpError);
        };
        $scope.searchSegment = function (seg) {
            saveSearchHistory($scope.data.searchkey);
            $ionicLoading.show({ template: 'Loading products from ' + seg.name });
            $scope.data.showSuggestion = false;
            $scope.data.showProducts = true;
            $http.get('http://' + $config.IP_PORT + '/product/segment/' + seg.id + '/searchKey/' + $scope.data.searchkey + '/retailer/' + $customlocalstorage.getObjectorDefault('defaultRetailer', '{}').id).then(function (res) {
                $ionicLoading.hide();
                $scope.data.Products = res.data;
                $scope.data.Products.forEach(function (e) { e.qty = $scope.returnQty(e); });
                $ionicScrollDelegate.scrollTop();
            }, httpError);
        };
        $scope.searchCategory = function (cat) {
            saveSearchHistory($scope.data.searchkey);
            $ionicLoading.show({ template: 'Loading products from ' + cat.name });
            $scope.data.showSuggestion = false;
            $scope.data.showProducts = true;
            $http.get('http://' + $config.IP_PORT + '/product/category/' + cat.id + '/searchKey/' + $scope.data.searchkey + '/retailer/' + $customlocalstorage.getObjectorDefault('defaultRetailer', '{}').id).then(function (res) {
                $ionicLoading.hide();
                $scope.data.Products = res.data;
                $scope.data.Products.forEach(function (e) { e.qty = $scope.returnQty(e); });
                $ionicScrollDelegate.scrollTop();
            }, httpError);
        };
    }])

    .controller("categoryCtrl", ["$scope", "$state", "$customlocalstorage", "$http", "ionicToast", '$config', '$filter', "$ionicLoading", "$sce", "$ionicHistory", "$stateParams", "$timeout", "$ionicSlideBoxDelegate", function ($scope, $state, $customlocalstorage, $http, ionicToast, $config, $filter, $ionicLoading, $sce, $ionicHistory, $stateParams, $timeout, $ionicSlideBoxDelegate) {
        $scope.category = [];
        $scope.segments = [];
        $scope.subsegments = [];
        $scope.products = [];
        $scope.view = {
            category: true,
            segments: false,
            subsegments: true,
            products: true
        }
        $scope.selected = {
        };
        $scope.loadCategory = function () {
            $scope.view.segments = false;
            $scope.view.subsegments = false;
            $scope.view.products = false;
            $ionicLoading.show({ template: "Loading category.....", duration: 10 });
            $http.get('http://' + $config.IP_PORT + '/category').then(function (res) {
                $scope.category = res.data;
                $ionicLoading.hide();
            }, function (err) {
                console.log(err);
            });
        };
        $scope.loadSegments = function (cat, event) {
            $scope.view.segments = true;
            $scope.view.subsegments = false;
            $scope.view.products = false;
            $(event.target).addClass('selected');
            $(event.target).siblings().removeClass('selected');
            $scope.selected.id1 = cat.id;
            $ionicLoading.show({ template: "Loading segments.....", duration: 10 });
            $http.get('http://' + $config.IP_PORT + '/category/segment/' + cat.id).then(function (res) {
                $scope.segments = res.data;
                $ionicLoading.hide();
            });
        };
        $scope.loadSubSegments = function (seg, event) {
            $scope.view.subsegments = true;
            $scope.view.products = false;
            $(event.target).addClass('selected');
            $(event.target).siblings().removeClass('selected');
            $scope.selected.id2 = seg.id;
            $ionicLoading.show({ template: "Loading segments.....", duration: 10 });
            $http.get('http://' + $config.IP_PORT + '/category/segment/subsegment/' + seg.id).then(function (res) {
                $scope.subsegments = res.data;
                $ionicLoading.hide();
            });
        };
        $scope.loadProducts = function (subseg, event) {
            $scope.products = [];
            $scope.view.products = true;
            $(event.target).addClass('selected');
            $(event.target).siblings().removeClass('selected');
            $ionicLoading.show({ template: "Loading products.....", });
            $http.get('http://' + $config.IP_PORT + '/product/category/' + $scope.selected.id1 + '/segment/' + $scope.selected.id2 + '/subsegment/' + subseg.id)
            .then(function (res) {
                $ionicLoading.hide();
                $scope.products = res.data.customeProducts == null ? [] : res.data.customeProducts;

                $scope.products.forEach(function (e) { e.qty = $scope.returnQty(e); });
            });
        }
        $scope.finishedRendering = function () {
            console.log('rendered');
        };

        $scope.onSlideMove = function (data) {
            console.log($scope.category);
        };
        $scope.loadCategory();
        //$scope.slideHasChanged = function (index) {
        //    console.log(index);
        //}
        $scope.goBySegment = function (segid, catid) {
            $state.go('app.subsegment', { segmentid: segid, categoryid: catid });
        };
    }])

    .controller('dailyNeedsCtrl', ['$scope', '$state', '$config', '$http', '$ionicLoading', '$customlocalstorage', '$filter', '$ionicScrollDelegate', function ($scope, $state, $config, $http, $ionicLoading, $customlocalstorage, $filter, $ionicScrollDelegate) {
        PageButtonClicked("Daily Needs");
        $scope.data = {
        }
        $scope.selected = {
        };
        $scope.view = {
            category: true,
            segments: false,
            subsegments: true,
            products: true
        }
        $ionicLoading.show({
            template: 'Loading daily needs categories...',
            duration: 10000
        });
        $http.get('http://' + $config.IP_PORT + '/category/daily/retailer/' + $customlocalstorage.getObjectorDefault('defaultRetailer', '[]').id).then(function (res) {
            $scope.data.categoryTree = res.data;
            $ionicLoading.hide();

        }, httpError);
        $scope.loadSegments = function (cat, event) {
            $scope.view.segments = true;
            $scope.view.subsegments = false;
            $scope.view.products = false;
            $(event.target).addClass('selected');
            $(event.target).siblings().removeClass('selected');
            $scope.selected.id1 = cat.id;
            $scope.segments = cat.segments;
            $ionicScrollDelegate.$getByHandle('segementScrollHandle').scrollTo(0, 0, true);
        };
        $scope.loadSubSegments = function (seg, event) {
            $scope.view.subsegments = true;
            $scope.view.products = false;
            $(event.target).addClass('selected');
            $(event.target).siblings().removeClass('selected');
            $scope.selected.id2 = seg.id;
            $scope.subsegments = seg.subsegments;
            $ionicScrollDelegate.$getByHandle('subsegmentScrollHandle').scrollTo(0, 0, true);
        };
        $scope.loadProducts = function (subseg, event) {
            $scope.products = [];
            $scope.view.products = true;
            $(event.target).addClass('selected');
            $(event.target).siblings().removeClass('selected');
            $ionicLoading.show({ template: "Loading products.....", });
            $http.get('http://' + $config.IP_PORT + '/product/category/' + $scope.selected.id1 + '/segment/' + $scope.selected.id2 + '/subsegment/' + subseg.id + '/retailer/' + $customlocalstorage.getObjectorDefault('defaultRetailer', '{}').id)
            .then(function (res) {
                $ionicLoading.hide();
                $scope.products = res.data.customeProducts == null ? [] : res.data.customeProducts;
                $scope.products.forEach(function (e) { e.qty = $scope.returnQty(e); });
            });
        }
    }])

    .controller('nondailyNeedsCtrl', ['$scope', '$state', '$config', '$http', '$ionicLoading', '$customlocalstorage', '$ionicScrollDelegate', function ($scope, $state, $config, $http, $ionicLoading, $customlocalstorage, $ionicScrollDelegate) {
        PageButtonClicked("Non-daily Needs");
        $scope.data = {
        }
        $scope.selected = {
        };
        $scope.view = {
            category: true,
            segments: false,
            subsegments: true,
            products: true
        }
        $ionicLoading.show({
            template: 'Loading non-daily needs categories...',
            duration: 10000
        });
        $http.get('http://' + $config.IP_PORT + '/category/nondaily/retailer/' + $customlocalstorage.getObjectorDefault('defaultRetailer', '[]').id).then(function (res) {
            $scope.data.categoryTree = res.data;
            $ionicLoading.hide();
        }, httpError);
        $scope.loadSegments = function (cat, event) {
            $scope.view.segments = true;
            $scope.view.subsegments = false;
            $scope.view.products = false;
            $(event.target).addClass('selected');
            $(event.target).siblings().removeClass('selected');
            $scope.selected.id1 = cat.id;
            $scope.segments = cat.segments;
            //$ionicScrollDelegate.$getByHandle('segementScrollHandle').scrollTo(0, 0, true);
        };
        $scope.loadSubSegments = function (seg, event) {
            $scope.view.subsegments = true;
            $scope.view.products = false;
            $(event.target).addClass('selected');
            $(event.target).siblings().removeClass('selected');
            $scope.selected.id2 = seg.id;
            $scope.subsegments = seg.subsegments;
            //$ionicScrollDelegate.$getByHandle('subsegmentScrollHandle').scrollTo(0, 0, true);
        };
        $scope.loadProducts = function (subseg, event) {
            $scope.products = [];
            $scope.view.products = true;
            $(event.target).addClass('selected');
            $(event.target).siblings().removeClass('selected');
            $ionicLoading.show({ template: "Loading products.....", });
            $http.get('http://' + $config.IP_PORT + '/product/category/' + $scope.selected.id1 + '/segment/' + $scope.selected.id2 + '/subsegment/' + subseg.id + '/retailer/' + $customlocalstorage.getObjectorDefault('defaultRetailer', '{}').id)
            .then(function (res) {
                $ionicLoading.hide();
                $scope.products = res.data.customeProducts == null ? [] : res.data.customeProducts;
                $scope.products.forEach(function (e) { e.qty = $scope.returnQty(e); });
            });
        }
    }])

    .controller("subsegmentCtrl", ["$scope", "$state", "$customlocalstorage", "$http", "ionicToast", '$config', '$filter', "$ionicLoading", "$sce", "$ionicHistory", "$stateParams", "$timeout", "$ionicSlideBoxDelegate", function ($scope, $state, $customlocalstorage, $http, ionicToast, $config, $filter, $ionicLoading, $sce, $ionicHistory, $stateParams, $timeout, $ionicSlideBoxDelegate) {
        $scope.subsegments = [];
        $scope.loadSubsegments = function () {
            $http.get('http://' + $config.IP_PORT + '/category/segment/subsegment/' + $stateParams.segmentid).then(function (res) {
                $scope.subsegments = res.data;
                angular.forEach($scope.subsegments, function (value, index) {
                    $http.get('http://' + $config.IP_PORT + '/product/category/' + $stateParams.categoryid + '/segment/' + $stateParams.segmentid + '/subsegment/' + value.id)
                    .then(function (res) {
                        $scope.subsegments[index].products = res.data.customeProducts == null ? [] : res.data.customeProducts;
                        $ionicSlideBoxDelegate.update();
                    }, function (err) {
                    });
                });
            }, function (err) {
                console.log(err);
            });
        };
        $scope.finishedRendering = function () {
            console.log('rendered');
        };

        $scope.onSlideMove = function (data) {
            console.log($scope.category);
        };
        $scope.loadSubsegments();


    }])

    .controller("productcategoryCtrl", ["$scope", "$state", "$customlocalstorage", "$http", "ionicToast", '$config', '$filter', "$ionicLoading", "$sce", "$ionicHistory", "$stateParams", "$timeout", function ($scope, $state, $customlocalstorage, $http, ionicToast, $config, $filter, $ionicLoading, $sce, $ionicHistory, $stateParams, $timeout) {
        PageButtonClicked("Products");
        $scope.data = { searchkey: '' };
        $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            if (fromState.name != 'app.productcategory') {
                $scope.loadProductByCategory($state.params.id1, $state.params.id2, $state.params.id3, $state.params.subSegment);
            }
        });
        $scope.searchList = [];
        $scope.productCount = 0;
        $scope.searchList = [];
        $scope.productDetailList = [];
        $scope.productRankedList = [];
        var getSuggRegDone = false;
        $scope.loadProductByCategory = function (id1, id2, id3, subSegName) {
            $scope.productDetailList = [];
            $ionicLoading.show({
                template: "Loading products.....",
            });

            $http.get('http://' + $config.IP_PORT + '/product/paid/category/' + id1 + '/segment/' + id2 + '/subsegment/' + id3 + '/retailer/' + $customlocalstorage.getObjectorDefault('defaultRetailer', '{}').id)
            .then(function (res) {
                $scope.productRankedList = res.data.customeProducts == null ? [] : res.data.customeProducts;
                $scope.productRankedList.forEach(function (e) { e.qty = $scope.returnQty(e); });
            }, function (err) {

            })

            $http.get('http://' + $config.IP_PORT + '/product/category/' + id1 + '/segment/' + id2 + '/subsegment/' + id3 + '/retailer/' + $customlocalstorage.getObjectorDefault('defaultRetailer', '{}').id)
            .then(function (res) {
                $ionicLoading.hide();
                $scope.productDetailList = res.data.customeProducts == null ? [] : res.data.customeProducts;
                $scope.productDetailList.forEach(function (e) { e.qty = $scope.returnQty(e); });
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.loadProductByCategory($stateParams.id1, $stateParams.id2, $stateParams.id3, $stateParams.subSegName);
    }])

    .controller("productDetailCtrl", ["$scope", "$stateParams", function ($scope, $stateParams) {
        PageButtonClicked("Product Details");
        $scope.product = $stateParams.product;
        if ($scope.product.imagesCount == 0) {
            $scope.product.imagesCount = 2;
        }
        $scope.product.imageArray = [];
        for (var i = 1 ; i <= Number($scope.product.imagesCount) ; i++) {
            $scope.product.imageArray.push(i);
        }
    }])

    .controller("addToCartCtrl", ["$scope", "$state", "$customlocalstorage", "$http", "$filter", "$popupService", "$stringResource", "ionicToast", '$config', '$ionicLoading', '$rootScope',
    function ($scope, $state, $customlocalstorage, $http, $filter, $popupService, $stringResource, ionicToast, $config, $ionicLoading, $rootScope) {
        PageButtonClicked("Add To Cart");
        $scope.displayProductDetailList = [];
        $scope.grandTotal = 0;
        $scope.view = {
            noItems: false
        };
        $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            toState.name == "app.addToCart" && loadCartItems();
        });
        $scope.$watch('displayProductDetailList', function (obj) {
            $scope.grandTotal = 0;
            obj.forEach(function (e) {
                $scope.grandTotal += (angular.isNumber(e.mrp) ? e.mrp : 0) * (angular.isNumber(e.qty) ? e.qty : 0);
            });
        });
        $scope.checkQty = function () {
            $scope.displayProductDetailList = $scope.displayProductDetailList.filter(function (e) { return e.qty != 0; });
        };
        var loadCartItems = function () {
            $scope.displayProductDetailList = [];
            $scope.view.noItems = $customlocalstorage.getObjectorDefault('cartlist', '[]').length == 0;
            angular.forEach($customlocalstorage.getObjectorDefault('cartlist', '[]'), function (value, index) {
                $ionicLoading.show({
                    template: "Loading Items...",
                    duration: 5000
                });
                var req = {
                    method: 'GET',
                    url: 'http://' + $config.IP_PORT + '/product/' + value.productId,
                }
                $http(req).then(function (res) {
                    res.data.customeProducts[0].qty = value.Qty;
                    $scope.displayProductDetailList.push(res.data.customeProducts[0]);
                    var e = res.data.customeProducts[0];
                    $scope.grandTotal += (angular.isNumber(e.mrp) ? e.mrp : 0) * (angular.isNumber(e.qty) ? e.qty : 0);
                    $ionicLoading.hide();
                });
            });
        }
        $scope.data = {
            confirm: false
        };
        var placeThisOrder = function () {
            $popupService.showConfirm("Order Confirmation", $stringResource.getValue("orderconfirm"), $scope.data)
            .then(function () {
                if ($scope.data.confirm) {
                    $ionicLoading.show({
                        template: "Placing Order. Please wait...",
                    });
                    var cartList = $customlocalstorage.getObjectorDefault('cartlist', '[]');
                    if (cartList.length <= 0) {
                        $popupService.showAlert("No Items in the Cart! Please add some items");
                    }
                    var orderData = {
                        orderDate: $filter('date')(new Date(), 'dd-MM-yyyy HH:mm:ss'),
                        consumerId: $config.getCustomerId(),
                        status: 0,
                        total:$scope.grandTotal,
                        orderRequiredDate: $filter('date')(new Date(), 'dd-MM-yyyy HH:mm:ss'),
                        orderItems: []
                    };

                    angular.forEach(cartList, function (value, index) {
                        orderData.orderItems.push({
                            productId: value.productId,
                            productName: "",
                            quantity: value.Qty
                        });
                    });

                    var orderReq = {
                        url: 'http://' + $config.IP_PORT + '/order/placeOrder',
                        method: "POST",
                        data: JSON.stringify(orderData)
                    };
                    console.log(orderData);
                    $http(orderReq).then(function (res) {
                        console.log(res);
                        if (res.data.status == '1') {
                            $popupService.showAlert('Order Request', 'Your order ' + res.data.orderId + ' is placed successfully. Mode of payment remains as usual.');
                            localStorage['lastOrderID'] = res.data.orderId;
                            localStorage.removeItem("cartlist");
                            $scope.parentObj.cartCount = 0;
                            $state.go('app.products');
                        }
                        else if (res.data.status == '-1') {

                            var products = [];
                            var messageText = "<ol>";
                            angular.forEach(res.data.orderItems, function (pe, pi) {
                                if (!pe.isItemOK) {
                                    var prod = $scope.displayProductDetailList.filter(function (e) { return e.id == pe.productId; })[0];
                                    products.push(prod);
                                    messageText += "<li>" + prod.name + "</li>";
                                }
                            });
                            messageText += "</ol>";
                            $popupService.showAlert('Some items are unavailable', '<b>Following items are not available for the current retailer:</b> ' + messageText);
                        }
                        else {
                            $popupService.showAlert('Order Request', 'Unable to submit the order!(Status:' + res.data.status + ')');
                        }
                        $ionicLoading.hide();
                    }, function (data) {
                        console.log(data);
                    });
                }
            });
        }
        $scope.placeOrder = function () {
            if ($customlocalstorage.getObjectorDefault('cartlist', '[]').length == 0) {
                ionicToast.show('Please add some products to Cart.', 'middle', false, 3000);
            }
            else {
                placeThisOrder();
            }
        };
    }])

    .controller("reminderCtrl", ["$scope", "$state", "$customlocalstorage", "$http", "$filter", "$popupService", "$stringResource", '$config', "ionicToast", "$ionicPopover", function ($scope, $state, $customlocalstorage, $http, $filter, $popupService, $stringResource, $config, ionicToast, $ionicPopover) {
        PageButtonClicked("Reminders");
        $scope.wishlist = [];
        $scope.data = {};
        $scope.optionPopover = null;
        $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            $scope.loadWishlist();
        });
        $scope.addWishlistToCart = function (product) {
            var cartList = $customlocalstorage.getObjectorDefault('cartlist', '[]');
            var obj = $filter('GetObject')('productId', product.id, cartList);
            if (obj == null) {
                cartList.push({
                    'productId': product.id,
                    'Qty': product.qty
                });
            }
            else {
                cartList[obj.index].Qty += product.qty;
            }
            $customlocalstorage.setObject('cartlist', cartList);
        };
        $scope.addAllToCart = function () {
            $popupService.showConfirm('Confirm', 'Do want to add all items to Cart?', $scope.data).then(function () {
                if ($scope.data.confirm) {
                    angular.forEach($scope.wishlist, function (value, index) {
                        $scope.addWishlistToCart(value);
                    });
                    $scope.wishlist = [];
                    $customlocalstorage.setObject('wishlist', $scope.wishlist);
                    ionicToast.show('Item(s) added to Cart.', 'bottom', false, 3000);
                }
                else {
                    ionicToast.show('Item(s) not moved to Cart.', 'bottom', false, 3000);
                }
            });
        };
        $scope.addSelectedItemsTOCart = function () {
            var tempList = [];
            angular.forEach($scope.wishlist, function (value, index) {
                console.log(value.class);
                if (value.class == "selected") { $scope.addWishlistToCart(value); }
                else { tempList.push(value); }
            });
            var count = $scope.wishlist.length - tempList.length;
            if (count == 0) {
                ionicToast.show('Please select some items', 'bottom', false, 3000);
            }
            else {
                ionicToast.show(count + ' item(s) added to Cart.', 'bottom', false, 3000);
            }
            $scope.wishlist = tempList;
            $customlocalstorage.setObject('wishlist', $scope.wishlist);
        };
        $scope.removeSelectedItems = function () {
            var count = $scope.wishlist.length - ($.grep($scope.wishlist, function (e) { return e.class != "selected" }).length);
            if (count == 0) {
                ionicToast.show('Please select items to remove', 'bottom', false, 3000);
            }
            else {
                $scope.wishlist = $.grep($scope.wishlist, function (e) { return e.class != "selected" });
                $customlocalstorage.setObject('wishlist', $scope.wishlist);
                ionicToast.show(count + ' item(s) removed', 'bottom', false, 3000);
            }
        };
        $scope.selectAll = function () {
            $scope.wishlist.forEach(function (ele) { ele.class = "selected" });
        };
        $scope.deselectAll = function () {
            $scope.wishlist.forEach(function (ele) { ele.class = "" });
        };
        

        $scope.qtyChanged = function (item, change) {
            item.qty += change;
            if (item.qty == 0) {
                var obj = $filter('GetObject')('id', item.id, $scope.wishlist);
                $scope.wishlist.splice(obj.index, 1);
            }
            else if (item.qty > 999) {
                item.qty = 1;
            }
            $customlocalstorage.setObject('wishlist', $scope.wishlist);
        };
        $scope.loadWishlist = function () {
            $scope.wishlist = $customlocalstorage.getObjectorDefault('wishlist', '[]');
            if ($scope.wishlist.length == 0) {
                $scope.data.selectText = '';
            }
        };
        $scope.loadWishlist();
        $scope.onSelect = function (event, item) {
            item.class = item.class == "selected" ? "" : "selected";
        };
        $ionicPopover.fromTemplateUrl('remainder-option-popover.html', { scope: $scope }).then(function (popover) { $scope.optionPopover = popover; });
        $scope.openPopover = function ($event) { $scope.optionPopover.show($event); };
        $scope.closePopover = function () { $scope.optionPopover.hide(); };
    }])

    .controller("wishlistCtrl", ['$scope', 'ionicToast', '$http', '$config', '$ionicLoading', '$state', '$filter', function ($scope, ionicToast, $http, $config, $ionicLoading, $state, $filter) {
            PageButtonClicked("Wishlist");
            $scope.data = { whishlistContent: "" };
            $scope.submit = function () {
                if ($scope.data.whishlistContent.length < 1) {
                    ionicToast.show('Please enter some text as a wishlist!', 'bottom', false, 3000);
                }
                else {
                    $ionicLoading.show({
                        template: 'Submitting...',
                        duration: 3000
                    });
                    var reqObj = {
                        whishlistContent: $scope.data.whishlistContent,
                        date: $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss'),
                        customerId: $config.getCustomerId(),
                        uuid: device.uuid,
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded;' }
                    };
                    $http.put('http://' + $config.IP_PORT + '/wishlist/add/' + reqObj.date + '/' + reqObj.whishlistContent + '/' + reqObj.customerId).success(function () {
                        ionicToast.show('Wishlist is requested successfully.', 'bottom', false, 3000);
                        $scope.data.whishlistContent = "";
                        $ionicLoading.hide();
                        $state.go('app.products');
                    });
                }
            };
        }])

    .controller("editprofileCtrl", ["$scope", "$customlocalstorage", "$http", "$filter", "$cordovaImagePicker", "$cordovaContacts", "ionicToast", '$cordovaCamera', '$popupService', '$config', '$ionicPopup', "$ionicLoading", "$cordovaDatePicker", function ($scope, $customlocalstorage, $http, $filter, $cordovaImagePicker, $cordovaContacts, ionicToast, $cordovaCamera, $popupService, $config, $ionicPopup, $ionicLoading, $cordovaDatePicker) {
        PageButtonClicked("Edit Profile");
        $scope.image = {
            progress: 0,
            currentImage: '',
            consumerImageUrl: "http://" + $config.IP_PORT + "/consumer/image/" + localStorage['consumerId']
        };
        $scope.profile = $customlocalstorage.getObject('consumer');
        $scope.profile.password = !$scope.profile.password && "";
        $scope.profile.zip_code = Number($scope.profile.zip_code);
        if ($scope.profile.gender == "Female") {
            $scope.profile.gender = "F";
        }
        else { $scope.profile.gender = "M"; }


        $scope.getDate = function () {

            var options = {
                date: new Date(),
                mode: 'date', // or 'time'
                minDate: new Date(),
                maxDate: new Date(),
                allowOldDates: false,
                allowFutureDates: false,
                doneButtonLabel: 'DONE',
                doneButtonColor: '#F2F3F4',
                cancelButtonLabel: 'CANCEL',
                cancelButtonColor: '#000000'
            };
            $cordovaDatePicker.show(options).then(function (date) {
                console.log(date);
                $scope.form.dateOfBirth = $filter('date')(new Date(date), 'dd-MM-yyyy');

            }, function (err) {
                console.log(err);
                $popupService.showAlert('Date picker error', JSON.stringify(err));
            });
        };
        $scope.originalProfile = {};
        $scope.imageData = '';
        var selectionPopup;
        var options = {};
        $scope.updateImage = function () {
            $scope.data = {};
            var uploadImage = function () {
                $cordovaCamera.getPicture(options).then(function (imageData) {
                    console.log(imageData);
                    $scope.imageData = imageData;

                    var opt = new FileUploadOptions();
                    opt.fileKey = "image_file";
                    opt.fileName = imageData.substr(imageData.lastIndexOf('/') + 1);
                    opt.mimeType = "image/jpeg";
                    opt.params = {
                        consumer_id: localStorage['consumerId']
                    };

                    var ft = new FileTransfer();
                    ft.onprogress = function (progressEvent) {
                        $scope.image.progress = JSON.stringify(progressEvent);
                        console.log(progressEvent);
                    };

                    $ionicLoading.show({
                        template: 'Uploading profile pic...',
                        duration: 20000
                    })
                    ft.upload(imageData, encodeURI("http://" + $config.IP_PORT + "/consumer/uploadImageFile"),
                    function (res) {
                        console.log(res);
                        $ionicLoading.hide();
                        ionicToast.show('Profile image uploaded.', 'bottom', false, 3000);

                        $scope.image.consumerImageUrl = "http://" + $config.IP_PORT + "/consumer/image/" + localStorage['consumerId'] + '?' + new Date().getTime();

                        var target = $('img#profile');
                        ImgCache.cacheFile($scope.image.consumerImageUrl);
                        ImgCache.useCachedFile(target)
                        //ImgCache.useOnlineFile(target);
                    },
                    function (err) {
                        console.log(err);
                        $ionicLoading.hide();
                        ionicToast.show('Error uploading the profile image. Please try again.', 'middle', false, 4000);
                    }, opt);

                }, function (err) {
                    console.log(err);
                    ionicToast.show('Error getting image from device.', 'bottom', false, 3000);
                });
            };
            $scope.getFromCamera = function () {
                selectionPopup.close();
                options = {
                    quality: 100,
                    destinationType: Camera.DestinationType.NATIVE_URI,
                    sourceType: Camera.PictureSourceType.CAMERA,
                    allowEdit: true,
                    encodingType: Camera.EncodingType.JPEG,
                    targetWidth: 512,
                    targetHeight: 512,
                    popoverOptions: CameraPopoverOptions,
                    saveToPhotoAlbum: true,
                    correctOrientation: true
                };
                uploadImage();
            };
            $scope.getFromFile = function () {
                selectionPopup.close();
                options = {
                    quality: 50,
                    destinationType: Camera.DestinationType.FILE_URI,
                    sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM,
                    allowEdit: true
                };
                uploadImage();
            };

            selectionPopup = $ionicPopup.show({
                template: '<div class="list card"><a class="item item-icon-left" ng-click="getFromCamera()"><i class="icon ion-camera"></i>Camera</a><a class="item item-icon-left" ng-click="getFromFile()"><i class="icon ion-image"></i>Existing file</a></div>',
                title: 'Take image from?',
                scope: $scope
            });
        };
        $scope.validateForm = function () {
            var popupContent = '';

            if (angular.isUndefined($scope.profile.name) || !$scope.profile.name.toString().match(/^[A-z\s]{3,}$/)) {
                popupContent = popupContent.concat("Name must have correct value");
            }
            else if (angular.isUndefined($scope.profile.date_of_birth) || !$scope.profile.date_of_birth.match(/^\d{1,2}-\d{1,2}-\d{4}$/)) {
                popupContent = popupContent.concat("Enter the correct DOB");
            }
            else if (angular.isUndefined($scope.profile.address) || !$scope.profile.address.match(/^[#A-z0-9\s]{3,}$/)) {
                popupContent = popupContent.concat("Enter the correct street name");
            }
            else if (!!!$scope.profile.zip_code || !$scope.profile.zip_code.toString().match(/^\d{6}$/)) {
                popupContent = popupContent.concat("Enter the correct zipcode");
            }

            if (popupContent !== '') {
                $popupService.showAlert('Validation Error<i class="icon item-icon-left"></i>', popupContent);
            }
            else {
                return true;
            }
            return false;
        }
        $scope.updateProfile = function () {

            if (!$scope.validateForm()) {
                return;
            }
            var updateProfileData = {
                id: Number(localStorage['consumerId']),
                name: $scope.profile.name,
                dateOfBirth: $filter('date')(new Date($scope.profile.date_of_birth.substring(6, 10), parseInt($scope.profile.date_of_birth.substring(3, 5)) - 1, parseInt($scope.profile.date_of_birth.substring(0, 2)) + 1), 'yyyy-MM-dd'),
                mailId: $scope.profile.mail_id,
                passWord: $scope.profile.password,
                gender: $scope.profile.gender,
                phoneNumber: $scope.profile.phoneNumber,
                residenceaddress: {
                    zipCode: $scope.profile.zip_code,
                    street: $scope.profile.address
                }
            }
            $ionicLoading.show({
                template: "Profile updating..."
            });
            $http.post('http://' + $config.IP_PORT + '/consumer/Update', JSON.stringify(updateProfileData)).then(function (res) {
                if (res.data.status == "1") {
                    $http.get('http://' + $config.IP_PORT + '/consumer/id/' + res.data.id).then(function (res) {
                        $customlocalstorage.setObject('consumer', res.data);
                        $scope.profile = res.data;
                        $scope.profile.date_of_birth = $filter('date')(new Date(res.data.date_of_birth), 'dd-MM-yyyy');

                    }, function (err) {
                        ionicToast.show('Unable to get consumer details after registration.', 'bottom', false, 3000);
                    });

                }
                ionicToast.show(res.data.message, 'bottom', false, 3000);
                $ionicLoading.hide();
            }, function (err) {
                ionicToast.show("Error updating profile, check connectivity.", 'bottom', false, 3000);
                $ionicLoading.hide();
            });
        };
    }])

    .controller("viewprofileCtrl", ["$scope", "$customlocalstorage", "$http", '$config', '$filter', '$ionicLoading', '$state', '$window', function ($scope, $customlocalstorage, $http, $config, $filter, $ionicLoading, $state, $window) {
        PageButtonClicked("View Profile");
        $scope.profileData = {};
        $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            if (fromState.name == 'app.editprofile') {
                loadProfile();
            }
        });
        var loadProfile = function () {
            $ionicLoading.show({
                template: 'Loading profile...'
            });
            $http.get('http://' + $config.IP_PORT + '/consumer/id/' + localStorage['consumerId']).then(function (res) {
                $ionicLoading.hide();
                $scope.profileData = res.data;
                if (res.data.gender == 'F') {
                    $scope.profileData.gender = "Female";
                }
                else { $scope.profileData.gender = "Male"; }

                $scope.profileData.date_of_birth = $filter('date')(new Date(res.data.date_of_birth), 'dd-MM-yyyy');
                $customlocalstorage.setObject('consumer', res.data);
                $scope.profileData.consumerImageUrl = 'http://' + $config.IP_PORT + '/consumer/image/' + localStorage['consumerId'] + '?' + new Date().getTime();

                var target = $('img#profile');
                ImgCache.isCached($scope.profileData.consumerImageUrl, function (path, success) {
                    if (success) {
                        ImgCache.useCachedFile(target);
                        console.log('image got from cache');
                    } else {
                        console.log('image not found in the catch');
                        ImgCache.cacheFile($scope.profileData.consumerImageUrl, function () {
                            ImgCache.useCachedFile(target);
                        });
                    }
                });
            }, function (err) {
                $ionicLoading.hide();
            });
        };
        loadProfile();
    }])

    .controller("updateProfileImageCtrl", ["$scope", "$customlocalstorage", "$http", "$cordovaImagePicker", "$cordovaContacts", "ionicToast", '$cordovaCamera', '$popupService', '$config', '$ionicPopup', "$ionicLoading", "$ionicHistory", "$state", function ($scope, $customlocalstorage, $http, $cordovaImagePicker, $cordovaContacts, ionicToast, $cordovaCamera, $popupService, $config, $ionicPopup, $ionicLoading, $ionicHistory, $state) {
        PageButtonClicked("Upload Profile Image");
        $scope.image = {
            progress: 0,
            currentImage: '',
            consumerImageUrl: "http://" + $config.IP_PORT + "/consumer/image/" + localStorage['consumerId'] + '?' + new Date().getTime()
        };
        $ionicHistory.clearHistory();
        $ionicHistory.clearCache();
        $scope.imageData = '';
        var selectionPopup;
        var options = {};
        $scope.uploadImage = function () {
            $scope.data = {};
            var uploadImage = function () {
                $cordovaCamera.getPicture(options).then(function (imageData) {
                    console.log(imageData);
                    $scope.imageData = imageData;

                    var opt = new FileUploadOptions();
                    opt.fileKey = "image_file";
                    opt.fileName = imageData.substr(imageData.lastIndexOf('/') + 1);
                    opt.mimeType = "image/jpeg";
                    opt.params = {
                        consumer_id: localStorage['consumerId']
                    };

                    var ft = new FileTransfer();
                    ft.onprogress = function (progressEvent) {
                        $scope.image.progress = JSON.stringify(progressEvent);
                        console.log(progressEvent);
                    };

                    $ionicLoading.show({
                        template: 'Uploading profile pic...',
                        duration: 20000
                    })
                    ft.upload(imageData, encodeURI("http://" + $config.IP_PORT + "/consumer/uploadImageFile"),
                    function (res) {
                        $ionicLoading.hide();
                        ionicToast.show('Profile image uploaded.', 'bottom', false, 3000);
                        $state.go('app.products');
                    },
                    function (err) {
                        console.log(err);
                        $ionicLoading.hide();
                        ionicToast.show('Error uploading the profile image. Please try again.', 'middle', false, 4000);
                    }, opt);

                }, function (err) {
                    console.log(err);
                    ionicToast.show('Error getting image from device.', 'bottom', false, 3000);
                });
            };
            $scope.getFromCamera = function () {
                selectionPopup.close();
                options = {
                    quality: 100,
                    destinationType: Camera.DestinationType.NATIVE_URI,
                    sourceType: Camera.PictureSourceType.CAMERA,
                    allowEdit: true,
                    encodingType: Camera.EncodingType.JPEG,
                    targetWidth: 512,
                    targetHeight: 512,
                    popoverOptions: CameraPopoverOptions,
                    saveToPhotoAlbum: true,
                    correctOrientation: true
                };
                uploadImage();
            };
            $scope.getFromFile = function () {
                selectionPopup.close();
                options = {
                    quality: 50,
                    destinationType: Camera.DestinationType.FILE_URI,
                    sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM,
                    allowEdit: true
                };
                uploadImage();
            };

            selectionPopup = $ionicPopup.show({
                template: '<div class="list card"><a class="item item-icon-left" ng-click="getFromCamera()"><i class="icon ion-camera"></i>Camera</a><a class="item item-icon-left" ng-click="getFromFile()"><i class="icon ion-image"></i>Existing file</a></div>',
                title: 'Take image from?',
                scope: $scope
            });
        };
    }])

    .controller("ordersCtrl", ["$scope", "$state", "$customlocalstorage", "$http", "$config", "$ionicLoading", "$filter", function ($scope, $state, $customlocalstorage, $http, $config, $ionicLoading, $filter) {
        PageButtonClicked("Orders");
        $ionicLoading.show({
            template: "Loading your orders...",
            duration: 10000
        });
        $scope.ordersList = [];
        $scope.allStatus = [];
        $scope.view = {
            noOrders: false
        }
        var customerID = $config.getCustomerId();
        $http.get('http://' + $config.IP_PORT + '/order/consumer/' + customerID).then(function (res) {
            $scope.ordersList = res.data;
            if ($scope.ordersList.length > 0)
                $scope.ordersList = $scope.ordersList.slice(0, $scope.ordersList.length - 1);
            $scope.allStatus = res.data[res.data.length - 1];
            $scope.view.noOrders = res.data[1].length == 0;
            $ionicLoading.hide();
        }, function (err) {
            console.log(err);
            $ionicLoading.hide();
        });
        $scope.showOrderItems = function (order) {
            $state.go('app.orderDetail', { orderID: order.orderId });
        };
        console.log('ordersCtrl');
    }])

    .controller("orderDetailCtrl", ['$scope', '$http', '$customlocalstorage', '$stateParams', '$config', '$ionicLoading', '$popupService', '$filter', '$ionicPopup', 'ionicToast',
    function ($scope, $http, $customlocalstorage, $stateParams, $config, $ionicLoading, $popupService, $filter, $ionicPopup, ionicToast) {
        PageButtonClicked("Order Details");
        $scope.orderDetail = {};
        $scope.grandTotal = 0;
        $scope.data = {};
        $ionicLoading.show({
            template: "Loading order details...",
        });
        console.log($stateParams.orderID);
        $http.get('http://' + $config.IP_PORT + '/order/order_id/' + $stateParams.orderID).then(function (res) {
            $scope.orderDetail = res.data;
            $scope.grandTotal = 0;
            $scope.orderDetail.orderItems.forEach(function (e) {
                $scope.grandTotal += (angular.isNumber(e.productPrice) ? e.productPrice : 0) * (angular.isNumber(e.quantity) ? e.quantity : 0);
            });

            $ionicLoading.hide();
        }, function (err) {
            console.log(err);
            $ionicLoading.hide();
            $popupService.showAlert('Network Error', 'Seems like you do not have proper internet connection!');
        });
        $scope.addAllToCart = function () {
            var orderData = {};
            $popupService.showConfirm('Add Confirm', 'Are you sure you want to add all the items to cart?', orderData).then(function () {
                console.log(orderData);
                if (orderData.confirm) {
                    var cartList = $customlocalstorage.getObjectorDefault('cartlist', '[]');
                    var itemsFound = false;
                    angular.forEach($scope.orderDetail.orderItems, function (value, indexG) {
                        var found = false;
                        var index = 0;
                        angular.forEach(cartList, function (v, i) {
                            if (v.productId === value.productId) {
                                found = true;
                                itemsFound = true;
                                index = i;
                            }
                        });
                        if (found) {
                            cartList[index].Qty += value.quantity;
                        }
                        else {
                            cartList.push({ productId: value.productId, Qty: value.quantity });
                        }
                    });
                    if (itemsFound) {
                        $popupService.showAlert('Warning!', 'Some items may have already there in cart. The Qty will be added if the item has already in cart.');
                    }
                    $scope.parentObj.cartCount = cartList.length;
                    $customlocalstorage.setObject('cartlist', cartList);
                }
            });


        };
        $scope.saveAsTemplate = function () {
            $ionicPopup.show({
                template: '<input type="text" ng-model="data.templateName">',
                title: 'Enter template name',
                scope: $scope,
                buttons: [
                  { text: 'Cancel' },
                  {
                      text: '<b>Save</b>',
                      type: 'button-positive',
                      onTap: function (e) {
                          if (!$scope.data.templateName) {
                              e.preventDefault();
                          } else {
                              var templates = $customlocalstorage.getObjectorDefault('templates', '[]');
                              var obj = $filter('GetObject')('name', $scope.data.templateName, templates)
                              if (obj) {
                                  ionicToast.show('Template name already exists. Please select different name.', 'bottom', false, 4000);
                                  e.preventDefault();
                                  return;
                              }
                              return $scope.data.templateName;
                          }
                      }
                  }
                ]
            }).then(function (res) {
                if (res) {
                    var templates = $customlocalstorage.getObjectorDefault('templates', '[]');
                    templates.push({ name: res, items: $scope.orderDetail.orderItems });
                    $customlocalstorage.setObject('templates', templates);
                    ionicToast.show(res + ' got saved', 'bottom', false, 3000);
                }
            });
        };
        $scope.received = function () {
            $http.put('http://' + $config.IP_PORT + '/order/changeStatus/' + $stateParams.orderID + '/RECEIVED').then(function (res) {
                if (res.data[0].status === 'OK') {
                    $ionicLoading.hide();
                    ionicToast.show('Order status is changed', 'middle', false, 3000);
                    $scope.orderDetail.status = "RECEIVED";
                }
                else {
                    $cordovaToast.showLongBottom('Unable to change the order status');
                }
            }, function (err) {
                $cordovaToast.showLongBottom('Error uploading status');
            });

        }
    }])

    .controller("settingsCtrl", ["$scope", "$state", "$customlocalstorage", "$http", "$popupService", function ($scope, $state, $customlocalstorage, $http, $popupService) {
        PageButtonClicked("Settings");
        console.log('settingsCtrl');

        $scope.localStoragePair = [];

        angular.forEach(localStorage, function (v, i) {
            $scope.localStoragePair.push({ key: i, value: v });
        });
        $scope.refresh = function () {
            $state.go('app.developer');
        };
        $scope.resetData = function () {
            $scope.data = {
                confirm: false
            }
            $popupService.showConfirm("Reset Confirm", "Are you sure you want to log out!", $scope.data).then(function () {
                if ($scope.data.confirm) {
                    localStorage.clear();
                    document.location.href = 'index.html';
                    //$popupService.showAlert("Success", "Reset Done!").then(function () {
                    // //$state.go('login');
                    // document.location.href = 'index.html';
                    //});
                }
            });
        }
    }])

    .controller("developerCtrl", ["$scope", "$state", "$customlocalstorage", "$http", "$popupService", function ($scope, $state, $customlocalstorage, $http, $popupService) {
        PageButtonClicked("Developer View");
        $scope.localStoragePair = [];
        $scope.data = {
            choice: !localStorage['host'] ? 'ablrz.com:8080' : localStorage['host']
        };
        $scope.change = function (host) {
            localStorage['host'] = host;
        };
        angular.forEach(localStorage, function (v, i) {
            $scope.localStoragePair.push({ key: i, value: v });
        });
    }])

    .controller("feedbackCtrl", ["$scope", "$state", "$http", "ionicToast", "$filter", "$config", function ($scope, $state, $http, ionicToast, $filter, $config) {
        PageButtonClicked("Feedback");
        $scope.form = {
            feedbackText: ""
        };
        console.log('feedbackCtrl called');
        $scope.submit = function () {
            if ($scope.form.feedbackText.length < 1) {
                ionicToast.show('Please enter some text as a feedback!', 'bottom', false, 3000);
            }
            else {
                var reqObj = {
                    feedbackText: $scope.form.feedbackText,
                    date: $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss'),
                    customerId: $config.getCustomerId(),
                    uuid: device.uuid,
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded;' }
                };
                $http.put('http://' + $config.IP_PORT + '/feedback/add/' + reqObj.date + '/' + reqObj.feedbackText + '/' + reqObj.customerId)
                .success(function () {
                    ionicToast.show('Thanks for your feedback!', 'bottom', false, 3000);
                    $scope.form.feedbackText = "";
                    $state.go('app.products');
                });
            }
        };
    }])

    .controller("contactUsCtrl", ["$scope", "$state", "$customlocalstorage", "$http", function ($scope, $state, $customlocalstorage, $http) {
        PageButtonClicked("Contact Us");
        console.log('contactUsCtrl');




    }])

    .controller("retailerProfile", ['$scope', '$http', '$config', '$customlocalstorage', function ($scope, $http, $config, $customlocalstorage) {
        $scope.data = {
            retailer: {}
        };
        $http.get('http://' + $config.IP_PORT + '/retailer/' + $customlocalstorage.getObjectorDefault('defaultRetailer', '{}').id).then(function (res) {
            $customlocalstorage.setObject('dafaultRetailer', res.data);
            $scope.data.retailer = res.data;
        })
    }])

    .controller('templateCtrl', ['$scope', '$state', '$customlocalstorage', '$http', '$config', '$ionicPopover', '$popupService', '$ionicPopup', '$filter', 'ionicToast', function ($scope, $state, $customlocalstorage, $http, $config, $ionicPopover, $popupService, $ionicPopup, $filter, ionicToast) {
        PageButtonClicked("Templates");
        $scope.templates = $customlocalstorage.getObjectorDefault('templates', '[]');
        $scope.data = {};
        $scope.gotoTemplateDetails = function (temp) {
            $state.go('app.templateDetail', { template: temp });
        }
        $scope.templateOptionsPopover = '';
        $ionicPopover.fromTemplateUrl('template-option.html', {
            scope: $scope
        }).then(function (popover) {
            $scope.templateOptionsPopover = popover;
        });
        $scope.openTemplateOptions = function ($event, item) {
            $scope.data.optionItem = item;
            $scope.templateOptionsPopover.show($event);
        }
        $scope.renameItem = function () {
            $scope.templateOptionsPopover.hide();
            $scope.data.templateName = $scope.data.optionItem.name;
            $ionicPopup.show({
                template: '<input type="text" ng-model="data.templateName">',
                title: 'Template name?',
                scope: $scope,
                buttons: [
                  { text: 'Cancel' },
                  {
                      text: '<b>Save</b>',
                      type: 'button-positive',
                      onTap: function (e) {
                          if (!$scope.data.templateName) {
                              e.preventDefault();
                          } else {
                              var templates = $customlocalstorage.getObjectorDefault('templates', '[]');
                              var obj = $filter('GetObject')('name', $scope.data.templateName, templates)
                              if (obj) {
                                  ionicToast.show('Template name already exists. Please select different name.', 'bottom', false, 4000);
                                  e.preventDefault();
                                  return;
                              }
                              return $scope.data.templateName;
                          }
                      }
                  }
                ]
            }).then(function (res) {
                if (res) {
                    var obj = $filter('GetObject')('name', $scope.data.optionItem.name, $scope.templates);
                    $scope.templates[obj.index].name = res;
                    $customlocalstorage.setObject('templates', $scope.templates);
                    ionicToast.show('Template renamed as ' + res, 'bottom', false, 3000);
                }
            });
        };
        $scope.deleteItem = function () {
            $scope.templateOptionsPopover.hide();
            $popupService.showConfirm('Delete Confirm', 'Are you sure you want to dete the ' + $scope.data.optionItem.name + '?', $scope.data).then(function () {
                if ($scope.data.confirm) {
                    $scope.templates = $scope.templates.filter(function (el) { return el.name != $scope.data.optionItem.name; });
                    $customlocalstorage.setObject('templates', $scope.templates);
                    ionicToast.show($scope.data.optionItem.name + ' got deleted', 'bottom', false, 3000);
                }
            });
        };
    }])

    .controller('templateDetailCtrl', ['$scope', '$state', '$customlocalstorage', '$http', '$config', '$stateParams', '$popupService', function ($scope, $state, $customlocalstorage, $http, $config, $stateParams, $popupService) {
        PageButtonClicked("Template Details");
        $scope.template = $stateParams.template;
        $scope.addAllToCart = function () {
            var orderData = {};
            $popupService.showConfirm('Add Confirm', 'You want to add all the items to cart?', orderData).then(function () {
                if (orderData.confirm) {
                    var cartList = $customlocalstorage.getObjectorDefault('cartlist', '[]');
                    var itemsFound = false;
                    angular.forEach($scope.template.items, function (value, indexG) {
                        var found = false;
                        var index = 0;
                        angular.forEach(cartList, function (v, i) {
                            if (v.productId === value.productId) {
                                found = true;
                                itemsFound = true;
                                index = i;
                            }
                        });
                        if (found) {
                            cartList[index].Qty += value.quantity;
                        }
                        else {
                            cartList.push({ productId: value.productId, Qty: value.quantity });
                        }
                    });
                    if (itemsFound) {
                        $popupService.showAlert('Warning!', 'Some items may have already there in cart. The Qty will be added if the item has already in cart.');
                    }
                    $scope.parentObj.cartCount = cartList.length;
                    $customlocalstorage.setObject('cartlist', cartList);
                }
            });
        }
    }])
})();